$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib'))
$LOAD_PATH.unshift(File.dirname(__FILE__))
$testing = true

RSpec.configure do |config|

  config.treat_symbols_as_metadata_keys_with_true_values = true
  config.run_all_when_everything_filtered = true

  config.filter_run integration: false 
  config.filter_run db: false 
  config.filter_run dde: false 
end

NEW_DAY_ARC_CAPTION = 'new day arc'

require 'simplecov'
SimpleCov.start

require 'rspec'
require 'rspec/mocks'
require 'rspec/expectations'

require 'stringio'


LOGOUT = $logout = StringIO.new
$stdout = StringIO.new

require 'askuhe_hef2_almaty'

require 'factory_girl'
require 'fileutils'

require "win/dde"
require "diff-lcs"

require_relative 'helpers/arc_test_data'

# Requires supporting files with custom matchers and macros, etc,
# in ./support/ and its subdirectories.
Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each {|f| require f}

Dir.glob("spec/steps/**/*steps.rb"){|f| load f, true }


FileUtils.rm('log') rescue
FileUtils.rm('cons') rescue


RSpec.configure do |config|



	config.include FactoryGirl::Syntax::Methods

	config.before(:each) do

		$logout.string = ''
		$stdout.string = ''
	end

	config.after(:all) do
		begin
			File.open('log','a') do |file|
				file.write $logout.string 
				file.write "\n"
			end

			File.open('cons', 'a') do |file|
				file.write $stdout.string
				file.write "\n"
			end
		rescue Exception => ex

			File.open('err', 'a' ) do |file|
				file.write "#{DateTime.now} -- #{ex.message}"
			end
		end
	end  
end


##---------##
## HELPERS ##
##=========##

	def read_cfg

		@cfg = YAML.load_file(File.dirname(__FILE__) + "/cfg/scaner.cfg.yaml")

		ENV['OPC_SERVER'] = @cfg[:opc_server]
		ENV['OPC_CLASS'] = @cfg[:opc_class]
		@cfg
	end
