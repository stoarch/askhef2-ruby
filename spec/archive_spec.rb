# encoding: utf-8

require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

	DAY_ARC_ID ||= 1
	DAY_ARC_CAPTION ||= 'day arc'

	TEST_ARC_ID = 1
	TEST_ARC_CAPTION = 'test arc'
	TEST_ID = 1
	TEST_CAPTION = 'test'
	TEST_VALUE = 'demo'

describe 'Archive' do

	before :all do

			@arc_data ||= ArcTestData.new
	end


	before :each do

		@arc = Archive.new
		@header = Archive::Header.new
		@row = Archive::Row.new
		@header_col = Archive::Header::Column.new
		@row_cell = Archive::Row::Cell.new
	end


	it "should provide attributes"  do

    @arc.rows.should be_kind_of Array

		@arc.id = TEST_ARC_ID
		@arc.caption = TEST_ARC_CAPTION 
		@arc.header =  @header
		@arc.rows << @row 

		@arc.id.should == TEST_ARC_ID
		@arc.caption.should == TEST_ARC_CAPTION
		@arc.header.should == @header
		@arc.rows[0].should == @row
	end

	context "::Header" do

		it "should set columns"  do

			@header.columns << @header_col
			@header.columns.size.should == 1
			@header.columns[0].should == @header_col
		end


		it "should make columns from array"  do

			@arc = Archive.new
			@arc.id = DAY_ARC_ID 
			@arc.caption = DAY_ARC_CAPTION

			arc_fields = @arc_data.structure

			@arc.header = Archive::Header.make_from( @arc.id, arc_fields )

			@arc.header.columns.each_with_index do |col, i|

				i.class.should == Fixnum
				col.class.should == Archive::Header::Column

				col.id.should == i
			  col.arc_id.should == @arc.id
				col.caption.should == arc_fields[i] 
			end
		end



		it "should have columns as array"  do

			@header.columns.should be_kind_of Array
		end
		# todo: make more free design (does not provide to user changes of internal structure)


		context "::Column" do

			before :each do
				@test_col = Archive::Header::Column.new
			end

			it "should set caption"  do

				@test_col.caption = TEST_CAPTION
				@test_col.caption.should == TEST_CAPTION
			end

			it "should set id"  do

				@test_col.id = TEST_ID
				@test_col.id.should == TEST_ID
			end

			it "should set arc id"  do

				@test_col.arc_id = TEST_ID
				@test_col.arc_id.should == TEST_ID
			end
		end
	end


	context "::Row" do

		it "should set cells"  do

			@row.cells << @row_cell 
			@row.cells.size.should == 1
			@row.cells[0].should == @row_cell
		end

		it "should create cells from array"  do

			@arc = Archive.new
			@arc.id = DAY_ARC_ID 
			@arc.caption = DAY_ARC_CAPTION

			arc_fields = @arc_data.structure

			@arc.header = Archive::Header.make_from( @arc.id, arc_fields )

			arc_data =  @arc_data.row_value

			@arc.rows = Archive::Row.make_from( @arc.id, @arc.header.columns, arc_data )


			@arc.rows.cells.each_with_index do |cl, i|

				i.class.should == Fixnum
				cl.class.should == Archive::Row::Cell

				cl.id.should == i
			  cl.arc_id.should == @arc.id
				cl.value.should == arc_data[i] 
				cl.column.should == @arc.header.columns[i]
			end
		end

		it "should have cells as array"  do

			@row.cells.should be_kind_of Array
		end

		context "::Cells" do

			before :each do

				@test_cell = Archive::Row::Cell.new
			end

			it "should set id"  do

				@test_cell.id = TEST_ID
				@test_cell.id.should == TEST_ID
			end

			it "should set arc id"  do

				@test_cell.arc_id = TEST_ID
				@test_cell.arc_id.should == TEST_ID
			end

			it "should set column"  do

				col = Archive::Header::Column

				@test_cell.column = col
				@test_cell.column.should == col
			end


			it "should set value"  do

				@test_cell.value = TEST_VALUE
				@test_cell.value.should == TEST_VALUE
			end

		end
	end
end
