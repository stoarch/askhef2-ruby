
FORM_FEED = 0x0C

describe "VzddeQuerier -> " do

  before :all do

    @arc_data = ArcTestData.new
  end

  before :each do

    @data_qry = VzddeQuerier.new
  end


  it "can query test dde items", :dde  do


    res = @data_qry.query('G06', '(6, 50083, 0, 0)')

    ## test ##

    res.should_not be nil
    res[0].should_not be nil
    res[0].encoding.name.should == 'Windows-1251'

    res = res[0].chomp

    res[-1].bytes.to_a.first.should == FORM_FEED 

    res.size.should == @arc_data.fields.size

    res.should == @arc_data.fields

    @data_qry.dde_error.should be nil
  end
end

