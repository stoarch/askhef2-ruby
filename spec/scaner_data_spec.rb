
describe "ScanerData -> " do

  it "can be created"  do

    ScanerData.new( 1 ).should_not be nil
  end

  it "value can be set"  do

    ScanerData.new( 1 ).value.should == 1
  end
end
