require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

describe "DBWriter ->" do

  before :all do

    @arc_data = ArcTestData.new

    @arc_header = @arc_data.structure 
  end

	before :each do

		$logout.string = ''
    $stdout.string = ''

		@writer = DBWriter.new
		@writer.cfg = read_cfg
	end

	it "should save values to DB", :db  do

		@writer.values = temp_values

		@writer.execute

		res = receive_res_from_db

		$logout.string.should_not match /ERROR/ # this is system errors
    $stdout.string.should_not match /Error/

    res.should == temp_values.map{|v| {value: v.value, date_received: time_from( v.date_query ), is_good: !!(v.is_good =~ /Good/)} }
	end


  ## ......... ##
  #  FUNCTIONS  #
  ###############

	#todo: Move helper methods to external module
	def connect_to_db

		Sequel.connect(
			@cfg[:db][:connstr]
		)
	end


	def test_date
		DateTime.new( 2013, 04, 05, 01, 02, 00 )
	end

	def new_value( caption, value, date_query, is_good )
#todo: move to factory
		str = OpenStruct.new
		str.caption = caption 
		str.value = value
		str.date_query = date_query
		str.is_good = is_good

		str
	end

	def temp_values
		[
			new_value( caption = "temp_c1", value = 1.1, date_query = test_date, is_good = 'Good'),
			new_value( caption = "pres_c1", value = 2.1, date_query = test_date, is_good = 'Good'),
			new_value( caption = "flowrate_c1", value = 3.1, date_query = test_date, is_good = 'Good')
		]
	end

	def time_from( date_query )
		Time.new( date_query.year, date_query.month, date_query.day, date_query.hour, date_query.minute, date_query.second )
	end


	def receive_res_from_db

		db = connect_to_db

		res = db[:values].select( :value, :date_received, :is_good ).reverse_order( :pkid ).first(3).sort{|a, b| a[:value] <=> b[:value]}

	ensure
		db.disconnect
		db = nil
	end



end
