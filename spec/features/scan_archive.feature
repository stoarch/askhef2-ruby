Feature: Scan archives

Background: 
  Given there is db connection
  And accessible vzljot sp thru dde


  Scenario: scan day archive

    When server scan day archive
    Then day archive data should be queried
    And day archive data should be saved in db
    And display should show archive scan status



  Scenario: scan month archive

    When server scan month archive
    Then month archive data should be queried
    And month archive data should be saved in db
    And display should show archive scan status

