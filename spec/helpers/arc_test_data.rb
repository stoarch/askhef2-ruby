class ArcTestData

	def fields

		@arc_fields ||= read_arc_fields
	end

	def row

		@arc_row ||= read_arc_row
	end

	def initialize
	end

  def structure

    fields.split("\f").map{|v| v.strip }[1..-1]
  end

  def row_value

    row.split("\f").map{|v| v.strip}[3..-1]
  end

	private

		def read_arc_row

			File.open('dbarc_f1.txt', 'r:cp1251'){|f| f.read }
		end

		def read_arc_fields

        File.open('dfields.txt', 'r:cp1251'){|f| f.read }.chomp
		end
end


