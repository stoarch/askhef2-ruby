#encoding: utf-8

require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

describe "ArcTestData ->" do

	subject { ArcTestData.new }

	it "should has valid fields"  do

				subject.structure[0].encoding.name.should == "Windows-1251"
	end

	it "should has valid row"  do

		subject.row_value[0].encoding.name.should == "Windows-1251"
	end
end
