
describe "OpcParm -> " do

  subject do

    FactoryGirl.build( :opc_parm )
  end

  before(:all) do


  end

  it "can set values"  do

    subject.caption.should == PARM_CAPTION
    subject.value.should == PARM_VAL
    subject.is_good.should == PARM_IS_GOOD
    subject.date_query.should == PARM_DATE
  end

  it "can convert values to string"  do

    subject.to_s.should == "#{PARM_CAPTION}   #{PARM_VAL}   #{PARM_IS_GOOD}   #{PARM_DATE.strftime('%d-%m-%Y %H:%M')}"
  end
end
