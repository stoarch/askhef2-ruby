
describe "ScanerError -> " do

  it "can be created"  do

    ScanerError.new( 'error' ).should_not be nil
  end

  it "must have message"  do

    error = ScanerError.new( 'error' )
    error.message.should == 'error'
  end
end
