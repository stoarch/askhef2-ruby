# encoding: utf-8

require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

TEST_DEV_CAPTION = 'testsr'
TEST_DEV_DEVNO = 6
TEST_DEV_TYPE_CODE = 77
TEST_DEV_TYPE_NAME = 'ТСРВ-024М'

TEST_PARM_CAPTION = ''
TEST_PARM_CODE = 50084

TEST_ARR_CODE = 2126
TEST_PARM_CAPTION = 'Суточный сводный архив'
TEST_BASE_LIST_CODE = 50084

VZ_CONFIG_FILE = "c:\\progra~1\\VzljotSP\\Spdef.mdb"


describe "VzSettingsReader" do

  subject { VzSettingsReader.new( read_cfg ) }

  it "should find config file" do

    subject.file_name.should == VZ_CONFIG_FILE
  end

  it "should read device number", :db do

    subject.group_for( TEST_DEV_CAPTION ).should == TEST_DEV_DEVNO 
  end

  it "should find param code for device", :db do

    subject.archive_code_for( TEST_DEV_CAPTION, TEST_PARM_CAPTION ).should == TEST_PARM_CODE
  end

  it "should read array code from Arrays table", :db  do

    subject.get_arr_code( TEST_DEV_TYPE_CODE, TEST_PARM_CAPTION ).should == TEST_ARR_CODE
  end

  it "should read base list code from BaseStringList", :db  do

    subject.get_base_list_code( TEST_ARR_CODE, TEST_PARM_CAPTION ).should == TEST_BASE_LIST_CODE
  end

  it "should read dev type code from Devices table", :db  do

    subject.get_type_code_from_devices( TEST_DEV_CAPTION ).should == TEST_DEV_TYPE_CODE
  end

end
