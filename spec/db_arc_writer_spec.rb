# encoding: utf-8

require File.expand_path(File.dirname(__FILE__) + '/spec_helper')



describe "DbArcWriter ->" do

	context "with Day archive ->" do

		before :each do

			$logout.string = ''
      $stdout.string = ''

      output = Proc.new do |query|
              case query 
              when /SELECT.*arc_cols/ then  
                [[{id: 1, arc_id: DAY_ARC_ID, caption: 'col1'}], [{id:3, arc_id: DAY_ARC_ID, caption: 'col3'}]]
              end
      end

			@mock_db = Sequel.mock( host: 'postgres', fetch: output )

			@writer = DbArcWriter.new
			@writer.db = @mock_db
			@writer.cfg = read_cfg
			@writer.arc = temp_day_arc
		end

		after :each do

			must_work_without_errors
		end

		it "should find archive record in db"  do


			@mock_db.fetch = {id: 1, name: DAY_ARC_CAPTION}

      @writer.should_not be nil
      @writer.should be_kind_of DbArcWriter

			@writer.execute

			@mock_db.sqls.should include(  
																	"SELECT * FROM \"archives\" WHERE (\"name\" = '#{DAY_ARC_CAPTION}') LIMIT 1" 
																 )

		end


		context "when storing archive structure to db ->" do

			before :each do

				arc_header_ds = @mock_db[:arc_cols]
				arc_header_ds._fetch = [
          {id: 1, arc_id: 1, caption: 'col1'},
					{id: 3, arc_id: 1, caption: 'col3'}
        ]
			end


			it "should check columns existence"   do

        @writer.should be_kind_of DbArcWriter

				@writer.execute

				@mock_db.sqls.should include(
					"SELECT * FROM \"arc_cols\" WHERE (\"arc_id\" = 1)"
				)

			end



			it "should add new columns to db"  do


        @writer.arc.header.arc_id = 1
				@writer.arc.header.columns = [{id: 1, caption: 'col1'}, {id: 2, caption: 'col2'}]

				@writer.execute

				must_work_without_errors

				@mock_db.sqls.should include(

					"INSERT INTO \"arc_cols\" (\"arc_id\", \"caption\") VALUES ((\"arc_id\" = 1) AND (\"caption\" = 'col2')) RETURNING \"id\""
				)
      end
		end


		it "should store archive record to db"  do

      # setup archive data (and header)
      @writer.arc.header.arc_id = 1
      @writer.arc.header.columns = [{id: 1, caption: 'col1'}, {id: 2, caption: 'col2'}]
      @writer.arc.rows = []
      @writer.arc.rows << Archive::Row.make_from( DAY_ARC_ID, @writer.arc.header.columns, [ 1.3, 1.2 ] )


      @writer.execute

      must_work_without_errors

      # check that db has new data 
      sqls = @mock_db.sqls

			sqls.should include(

					"INSERT INTO \"arc_vals\" (\"arc_id\", \"arc_col_id\", \"value\") VALUES (1, 1, 1.3) RETURNING \"id\""
				)

				sqls.should include(

					"INSERT INTO \"arc_vals\" (\"arc_id\", \"arc_col_id\", \"value\") VALUES (1, 2, 1.2) RETURNING \"id\""
				)
    end


		context "without record in db ->" do

			before :all do

				@mock_db = Sequel.mock( host: 'postgres' )

				@writer = DbArcWriter.new
				@writer.db = @mock_db
				@writer.cfg = read_cfg
				@writer.arc = temp_day_arc
			end


			it "should make archive record"  do


				@writer.arc.caption = NEW_DAY_ARC_CAPTION

				@writer.execute


				@mock_db.sqls.should include (

						"INSERT INTO \"archives\" (\"name\") VALUES ('#{NEW_DAY_ARC_CAPTION}') RETURNING \"id\""
				)

			end
		end


		context "when error on save" do
		end

	end


	## SUPPORT METHODS ##
	#####################


  def column( id, arc_id, caption )
#todo: make with factory
    Archive::Header::Column.new do |col|

      col.id = id
      col.arc_id = arc_id
      col.caption = caption
    end
  end


	def temp_day_arc

		@arc_data = ArcTestData.new

		@arc = Archive.new
		@arc.id = DAY_ARC_ID 
		@arc.caption = DAY_ARC_CAPTION

		arc_fields = @arc_data.structure

		@arc.header = Archive::Header.make_from( @arc.id, arc_fields )


		arc_data = @arc_data.row_value

		@arc.rows << Archive::Row.make_from( @arc.id, @arc.header.columns, arc_data )

		@arc
	end


	def must_work_without_errors

			$logout.string.should_not match /Error/
				$stdout.string.should_not match /Error/
				$logout.string.should_not match /ERROR/
				$stdout.string.should_not match /ERROR/
	end


	def connect_to_db

		@mock_db
	end

	def read_cfg

		@cfg = YAML.load_file(File.dirname(__FILE__) + "/cfg/scaner.cfg.yaml")

		ENV['OPC_SERVER'] = @cfg[:opc_server]
		ENV['OPC_CLASS'] = @cfg[:opc_class]
		@cfg
	end

	def test_date
		DateTime.new( 2013, 04, 05, 01, 02, 00 )
	end
end
