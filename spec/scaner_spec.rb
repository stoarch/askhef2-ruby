#encoding: utf-8

require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

require 'fileutils'

ARC_HEADER_ITEMS = 219
ARC_VAL_ITEMS = 219

PARM_CAPTION = 'Demo'
PARM_VAL = 1.3
PARM_IS_GOOD = 'Good'
PARM_DATE = DateTime.new( 2013, 3, 10, 13, 35, 00 )

## Factories ##
###############

FactoryGirl.define do

  factory :opc_parm do 

    caption PARM_CAPTION 
    value PARM_VAL 
    is_good PARM_IS_GOOD 
    date_query PARM_DATE 
  end
end

## Specs ##
###########


describe "Scaner ->" do

  before :all do
    Celluloid::Actor[:message_board] ||= MessageBoard.new
    Celluloid::Actor[:scaner] ||= Scaner.new
    @scaner = Celluloid::Actor[:scaner] 
  end

  before :each do 

    $logout.string = ''
    $stdout.string = ''
  end

  after :all do

  end


  shared_examples_for "scaner error handler" do


    it "should prints exception on screen"  do

      $stdout.string.should match 'Error'
    end

    it "should log error message" do

      $logout.string.should =~ /Error/
    end


    it "should send error to message board"  do

      Celluloid::Actor[:message_board].should have_new_error 

      Celluloid::Actor[:message_board].error_message.should =~ /Error/
    end
  end


  def test_parm
    FactoryGirl.build( :opc_parm )
  end

  def read_parms( res_parm )

    mock_loader = mock()
    mock_loader.should_receive(:query).and_return(res_parm)

    @scaner.data_loader = mock_loader

    parms = @scaner.read_parms 'DemoOPC'
  end


  def opc_parm( caption, value, is_good, date )

    parm = OpcParm.new
    parm.caption = caption
    parm.value = value
    parm.is_good = is_good
    parm.date_query = date

    parm
  end



  def aNAN
    s, e, m = rand(2), 2047, rand(2**52-1)+1
    [sprintf("%1b%011b%052b", s,e,m)].pack("B*").unpack("G").first
  end


  context "OPC values receiver -> " do

    context "when error in query we can handle it" do

      before :each do

        failure_loader = mock()
        failure_loader.should_receive(:query).and_raise("Error: Test error")
        @scaner.data_loader = failure_loader

        @scaner.read_parms 'DemoOPC'
      end

      it_behaves_like "scaner error handler"
    end




    it "should receive values from mock"  do

      parms = read_parms( [test_parm.to_s] )

      parms.should_not be_nil 
      parms.size.should == 1

      parms[0].class.should == OpcParm
      test_parm.class.should == OpcParm

      res_parm = parms[0]

      res_parm.caption.should == test_parm.caption
      res_parm.value.should == test_parm.value
      res_parm.is_good.should == test_parm.is_good
      res_parm.date_query.should == test_parm.date_query

      parms[0].should == test_parm

    end



    context "when errors in input" do

      it "should detect nil parms"  do

        parms = read_parms( nil )

        parms.should == []

      end


      it "should detect empty parms"  do

        parms = read_parms( [] )

        parms.should == []
      end



      context "when invalid parms" do


        it "should detect empty caption"  do

          parms = read_parms( ['   1,32   Good   12.03.2013 22:45'] )

          parms.should == []

        end


        it "should detect NAN value"  do

          parms = read_parms( ['Demo   ---   Good   12.03.2013 22:45'])

          parms.should == []

        end


        it "should detect unknown status"  do

          parms = read_parms( ['Demo   1,33   Unknown   12.03.2013 22:45'])

          parms.should == [opc_parm('Demo', 1.33, 'Unknown', DateTime.new( 2013, 03, 12, 22, 45 ))]
        end


        it "should detect malformed date"  do

          parms = read_parms( ['Demo   1,33  Good  23.23.2013 22:45'])

          parms.should == []
        end
      end
    end
  end



  context "Archive values receiver -> " do

    before :all do

      @arc_data = ArcTestData.new

      @arc_header = @arc_data.structure 

      @arc_vals = @arc_data.row_value
    end


    context "when error in query batch we can handle it" do

      before :each do

        failure_loader = mock()
        failure_loader.should_receive(:query_batch).and_raise("Error: Test error")
        @scaner.arch_loader = failure_loader

        arc = @scaner.read_raw_archive('G06', ['"0, 50084, 0, 0"'])
      end

      it_behaves_like "scaner error handler"
    end



    it "should query and parse test archive structure"  do


      @arc_data.fields.should_not be nil
      @arc_data.fields.should be_kind_of String 

      mock_query = mock()
      mock_query.should_receive(:query_batch).with('G06', '"0, 50084, 0, 0"').and_return( [@arc_data.fields] )

      @scaner.arch_loader = mock_query 

      header = @scaner.read_archive_header('G06', ['"0, 50084, 0, 0"'])

      must_work_without_errors

      header.should_not == nil 
      header.should be_kind_of Array 


      @arc_header.should_not be nil
      @arc_header.size.should == ARC_HEADER_ITEMS 

      @arc_header[0].should_not be nil
      @arc_header[0].encoding.name.should == 'Windows-1251'

      header.size.should == ARC_HEADER_ITEMS 

      header.should == @arc_header 
      header[0].encoding.to_s.should == 'Windows-1251'

    end



    it "should query and parse test archive data"  do


      @arc_vals.should_not be nil
      @arc_vals.size.should == ARC_VAL_ITEMS 

      mock_query = mock()
      mock_query.should_receive(:query_batch).with('G06', ['"0, 50084, 15041713, 0, 2013, 0"']).and_return( [@arc_data.row] )

      @scaner.arch_loader = mock_query 

      data = @scaner.read_archive_data('G06', ['"0, 50084, 15041713, 0, 2013, 0"'])

      must_work_without_errors

      data.should_not be nil
      data.should be_kind_of Array 

      data.should_not be nil
      data.size.should == ARC_VAL_ITEMS

      data.should == @arc_vals 
    end

  end


  ### TOOLS ###
  ###_______###

	def must_work_without_errors

			$logout.string.should_not match /Error/
				$stdout.string.should_not match /Error/
				$logout.string.should_not match /ERROR/
				$stdout.string.should_not match /ERROR/
	end
end

