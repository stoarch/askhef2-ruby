
describe "EScaner -> " do

  it "can be created"  do

    error = EScaner.new

    error.should_not be nil
  end

  it "can be raised"  do

    ->(){ raise EScaner.new }.should raise_error( EScaner )
  end

end
