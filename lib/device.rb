class Device

  attr_accessor :id, :caption

  def group

    "G#{sprintf( '%02d', id )}" 
  end
end
