#encoding: utf-8

class VzSettingsReader

  attr :cfg

  def initialize( acfg )

    @cfg = acfg

  end

  # readen from settings file name of vzljot Spdef.mdb
  def file_name

    @cfg[ :settings ][ :file_name ] if @cfg
  end


  def group_for( dev_caption )

    query_one_value_for( "select КодГруппы from Группы where НаименованиеГруппы = '#{ dev_caption }'" )
  end



  def archive_code_for( dev_caption, parm_caption )

		debug_info("vzsettings_reader:: dev: #{dev_caption} parm: #{parm_caption}")

    dev_type_code = get_type_code_from_devices( dev_caption )

		raise 'VZSR:: Unable to get dev_type_code' if dev_type_code.nil?

		debug_info "vzsettings_reader:: dev type: #{dev_type_code}"

		
    base_code = get_base_list_code( dev_type_code, parm_caption ) 

    raise 'VZSR:: Unable to get base_code' if base_code.nil?

    debug_info "vzsettings_reader:: base_code #{base_code}"

    base_code
  end


  def get_type_code_from_devices( dev_caption )

    query_one_value_for( "select КодТипаПрибора from Приборы where НазваниеПрибора = '#{dev_caption}'" )
  end


  def get_arr_code( dev_type_code, parm_caption )

    query_one_value_for( "select КодМассива from Массивы where ( КодТипаПрибора = #{dev_type_code}) and ( ИмяМассива = '#{parm_caption}' )" )
  end


  ## = Read lbase list code for device and parm
  #
  # parms:
  #  - dev_type_code - type code for device to search
  #  - parm_caption  - caption of archive (list)
  #
  def get_base_list_code( dev_type_code, parm_caption )

    query_one_value_for( "select КодСтрокиОсновногоСписка from ОсновнойСписокСтроки where Массив in (select КодМассива from Массивы where КодТипаПрибора = #{dev_type_code}) and (ИмяПараметра = '#{parm_caption}')" ) 
  end


  private 
  #########


  def query_one_value_for( sql )

    db = AccessDb.new( file_name )

    db.open

    db.query( sql )

    rows = db.data[0][0] if db.data && db.data[0]
  ensure

    db.close if db
  end
end
