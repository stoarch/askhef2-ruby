
class ScanDataCommand

	attr :cfg

	attr :old_time

	attr :obj_list

	attr :is_scaning
	attr :data_received

	alias :is_scaning? :is_scaning

	alias :data_received? :data_received


	def initialize( acfg )

		@cfg = acfg

		@obj_list = ScanObjectList.new( cfg )

		reset_scaner
	end


  def current_object_no

    @obj_list.find_index( current_object )

  end



	def execute

		debug_info "\n\n=================================================\n"
		debug_info "SCAN DATA:: start scan"

		reset_scaner

		start_scan_data 

		begin

			debug_info "SCAN DATA:: is_scaning #{is_scaning} and is_scaning? #{is_scaning}"

			if is_scaning?

				wait_until_data_received 

				begin

					store_data if data_received?

				end while message_board.has_data?  

				debug_info "SCAN DATA:: has data #{message_board.has_data?}"

			end

			query_next_object 

			yield self if block_given?

		end until all_objects_scanned? 

		reset_scaner

		debug_info "SCAN DATA:: Complete"
	end


	private
	####### SECTION

	def reset_scaner

		@is_scaning = false
		@data_received = false

		@old_time = Time.now

		@obj_list.reset

	end

	def all_objects_scanned?

		@obj_list.at_end?

	end


	def has_more_objects?

		@obj_list.has_more?

	end


	def at_start?

		@obj_list.at_start?

	end

	
	def move_to_next_object

		@obj_list.next

	end


	def message_board

		Celluloid::Actor[:message_board]

	end


	WAIT_TIMEOUT = 10 # seconds
	ITERATION_TIME = 0.5

	def wait_until_data_received

		status = Timeout::timeout( WAIT_TIMEOUT ) {

			begin

				sleep ITERATION_TIME 

			end until message_board.has_new_data? 

			message_board.has_new_data?
		}


		@data_received = status

	rescue Timeout::Error => ex

		debug_error "SCAN DATA:: timeout during wait for data"

		@data_received = false

	end


	def current_object 

		@obj_list.current

	end



	def query_next_object 

		@is_scaning = false

		move_to_next_object

		if has_more_objects? 

			debug_info "SCAN DATA:: Next query "

			params = current_object.get_params

			debug_error "SCAN DATA: Error : Params for #{current_object} is empty" if params == []

			return false if params == []

			debug_info "SCAN DATA:: curr object params capts #{params}"

			scaner.async.read_parms *params 

			@is_scaning = true
		end

	rescue Exception => ex

		debug_error "SCAN DATA:: Error #{ex.message}"

	end

	
	def scaner

		Celluloid::Actor[:scaner]

	end




	def clear_error_display

		$viewport.show_error_msg "None"

	end



	def start_scan_data

		@data = nil

		clear_error_display

		scaner.config = @cfg


		debug_info "SCAN DATA:: Scanning started..."
		debug_info "SCAN DATA:: obj #{current_object} of #{obj_list}"
		debug_info "SCAN DATA:: with parms: #{ current_object.get_params }"

		if current_object.get_params == []

			debug_error "SCAN DATA:: parms is empty for #{current_object}" 
			return false

		end

		scaner.async.read_parms *current_object.get_params 

		@is_scaning = true

		return true

	rescue Exception => ex

		debug_error "SCAN DATA:: Error #{ex}"

		$viewport.show_error_msg( ex )
	end


	def db_writer

		Celluloid::Actor[:db_writer]

	end


	def store_data

		@data = message_board.value

		return if @data.nil?

		$viewport.show_received_parms( @data )

		debug_info "SCAN DATA:: store data #{@data}"

		db_writer.values = @data

		db_writer.async.execute

		$logger.info "SCAN DATA:: Data storing"

	rescue Exception => ex

		debug_error ex.message

		debug_info $@

	end
end



