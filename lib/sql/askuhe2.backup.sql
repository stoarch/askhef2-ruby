--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.3
-- Dumped by pg_dump version 9.2.3
-- Started on 2013-03-31 12:37:18

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 174 (class 3079 OID 11727)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1950 (class 0 OID 0)
-- Dependencies: 174
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 169 (class 1259 OID 32775)
-- Name: pk_meter_id; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pk_meter_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pk_meter_id OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 168 (class 1259 OID 32769)
-- Name: meters; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE meters (
    id bigint NOT NULL,
    caption character varying(50) NOT NULL,
    pkid bigint DEFAULT nextval('pk_meter_id'::regclass) NOT NULL
);


ALTER TABLE public.meters OWNER TO postgres;

--
-- TOC entry 1951 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN meters.caption; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN meters.caption IS '
';


--
-- TOC entry 173 (class 1259 OID 32796)
-- Name: param_kinds; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE param_kinds (
    pkid integer NOT NULL,
    caption character varying(50)
);


ALTER TABLE public.param_kinds OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 32794)
-- Name: param_kinds_pkid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE param_kinds_pkid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.param_kinds_pkid_seq OWNER TO postgres;

--
-- TOC entry 1952 (class 0 OID 0)
-- Dependencies: 172
-- Name: param_kinds_pkid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE param_kinds_pkid_seq OWNED BY param_kinds.pkid;


--
-- TOC entry 171 (class 1259 OID 32788)
-- Name: values; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "values" (
    pkid bigint NOT NULL,
    meter_id bigint,
    value double precision,
    date_received timestamp without time zone,
    is_good boolean,
    chanel integer,
    parm_kind integer
);


ALTER TABLE public."values" OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 32786)
-- Name: values_pkid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE values_pkid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.values_pkid_seq OWNER TO postgres;

--
-- TOC entry 1953 (class 0 OID 0)
-- Dependencies: 170
-- Name: values_pkid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE values_pkid_seq OWNED BY "values".pkid;


--
-- TOC entry 1930 (class 2604 OID 32799)
-- Name: pkid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY param_kinds ALTER COLUMN pkid SET DEFAULT nextval('param_kinds_pkid_seq'::regclass);


--
-- TOC entry 1929 (class 2604 OID 32791)
-- Name: pkid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "values" ALTER COLUMN pkid SET DEFAULT nextval('values_pkid_seq'::regclass);


--
-- TOC entry 1937 (class 0 OID 32769)
-- Dependencies: 168
-- Data for Name: meters; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY meters (id, caption, pkid) FROM stdin;
\.


--
-- TOC entry 1942 (class 0 OID 32796)
-- Dependencies: 173
-- Data for Name: param_kinds; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY param_kinds (pkid, caption) FROM stdin;
\.


--
-- TOC entry 1954 (class 0 OID 0)
-- Dependencies: 172
-- Name: param_kinds_pkid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('param_kinds_pkid_seq', 1, false);


--
-- TOC entry 1955 (class 0 OID 0)
-- Dependencies: 169
-- Name: pk_meter_id; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pk_meter_id', 1, false);


--
-- TOC entry 1940 (class 0 OID 32788)
-- Dependencies: 171
-- Data for Name: values; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "values" (pkid, meter_id, value, date_received, is_good, chanel, parm_kind) FROM stdin;
494	1	26500	2013-03-30 22:28:45	t	1	1
495	1	0	2013-03-30 22:28:45	t	1	2
496	2	6334	2013-03-30 22:28:45	t	1	1
497	1	18467	2013-03-30 22:30:37	t	1	1
498	1	0	2013-03-30 22:30:37	t	1	2
499	2	41	2013-03-30 22:30:37	t	1	1
500	1	26500	2013-03-30 22:30:44	t	1	1
501	1	0	2013-03-30 22:30:44	t	1	2
502	2	6334	2013-03-30 22:30:44	t	1	1
503	1	18467	2013-03-30 22:32:15	t	1	1
504	1	0	2013-03-30 22:32:15	t	1	2
505	2	41	2013-03-30 22:32:15	t	1	1
506	1	26500	2013-03-30 22:32:58	t	1	1
507	1	0	2013-03-30 22:32:58	t	1	2
508	2	6334	2013-03-30 22:32:58	t	1	1
509	1	18467	2013-03-30 22:34:16	t	1	1
510	1	0	2013-03-30 22:34:16	t	1	2
511	2	41	2013-03-30 22:34:16	t	1	1
512	1	26500	2013-03-30 22:34:23	t	1	1
513	1	0	2013-03-30 22:34:23	t	1	2
514	2	6334	2013-03-30 22:34:23	t	1	1
515	1	26500	2013-03-30 23:44:21	t	1	1
516	1	0	2013-03-30 23:44:21	t	1	2
517	2	41	2013-03-30 23:44:21	t	1	1
518	2	18467	2013-03-30 23:44:21	t	3	1
519	2	6334	2013-03-30 23:44:21	t	3	1
520	3	19169	2013-03-30 23:44:21	t	2	1
521	1	26962	2013-03-30 23:44:29	t	1	1
522	1	0	2013-03-30 23:44:29	t	1	2
523	2	15724	2013-03-30 23:44:29	t	1	1
524	2	11478	2013-03-30 23:44:29	t	3	1
525	2	29358	2013-03-30 23:44:29	t	3	1
526	3	24464	2013-03-30 23:44:29	t	2	1
527	1	26500	2013-03-30 23:45:38	t	1	1
528	1	0	2013-03-30 23:45:38	t	1	2
529	2	41	2013-03-30 23:45:38	t	1	1
530	2	18467	2013-03-30 23:45:38	t	3	1
531	2	6334	2013-03-30 23:45:38	t	3	1
532	3	19169	2013-03-30 23:45:38	t	2	1
533	1	26962	2013-03-30 23:46:34	t	1	1
534	1	0	2013-03-30 23:46:34	t	1	2
535	2	15724	2013-03-30 23:46:34	t	1	1
536	2	11478	2013-03-30 23:46:34	t	3	1
537	2	29358	2013-03-30 23:46:34	t	3	1
538	3	24464	2013-03-30 23:46:34	t	2	1
539	1	26500	2013-03-31 00:13:48	t	1	1
540	1	0	2013-03-31 00:13:48	t	1	2
541	2	41	2013-03-31 00:13:48	t	1	1
542	2	18467	2013-03-31 00:13:48	t	3	1
543	2	6334	2013-03-31 00:13:48	t	3	1
544	3	19169	2013-03-31 00:13:48	t	2	1
545	1	26962	2013-03-31 00:14:30	t	1	1
546	1	0	2013-03-31 00:14:30	t	1	2
547	2	15724	2013-03-31 00:14:30	t	1	1
548	2	11478	2013-03-31 00:14:30	t	3	1
549	2	29358	2013-03-31 00:14:30	t	3	1
550	3	24464	2013-03-31 00:14:30	t	2	1
551	1	16827	2013-03-31 00:14:37	t	1	1
552	1	0	2013-03-31 00:14:37	t	1	2
553	2	5705	2013-03-31 00:14:37	t	1	1
554	2	28145	2013-03-31 00:14:37	t	3	1
555	2	23281	2013-03-31 00:14:37	t	3	1
556	3	9961	2013-03-31 00:14:37	t	2	1
557	1	4827	2013-03-31 00:15:05	t	1	1
558	1	0	2013-03-31 00:15:05	t	1	2
559	2	491	2013-03-31 00:15:05	t	1	1
560	2	2995	2013-03-31 00:15:05	t	3	1
561	2	11942	2013-03-31 00:15:05	t	3	1
562	3	5436	2013-03-31 00:15:05	t	2	1
563	1	153	2013-03-31 00:15:13	t	1	1
564	1	0	2013-03-31 00:15:13	t	1	2
565	2	32391	2013-03-31 00:15:13	t	1	1
566	2	14604	2013-03-31 00:15:13	t	3	1
567	2	3902	2013-03-31 00:15:13	t	3	1
568	3	292	2013-03-31 00:15:13	t	2	1
569	1	19718	2013-03-31 00:15:20	t	1	1
570	1	0	2013-03-31 00:15:20	t	1	2
571	2	12382	2013-03-31 00:15:20	t	1	1
572	2	17421	2013-03-31 00:15:20	t	3	1
573	2	18716	2013-03-31 00:15:20	t	3	1
574	3	19895	2013-03-31 00:15:20	t	2	1
575	1	26500	2013-03-31 00:17:19	t	1	1
576	1	0	2013-03-31 00:17:19	t	1	2
577	2	41	2013-03-31 00:17:19	t	1	1
578	2	18467	2013-03-31 00:17:19	t	3	1
579	2	6334	2013-03-31 00:17:19	t	3	1
580	3	19169	2013-03-31 00:17:19	t	2	1
581	1	26962	2013-03-31 00:17:27	t	1	1
582	1	0	2013-03-31 00:17:27	t	1	2
583	2	15724	2013-03-31 00:17:27	t	1	1
584	2	11478	2013-03-31 00:17:27	t	3	1
585	2	29358	2013-03-31 00:17:27	t	3	1
586	3	24464	2013-03-31 00:17:27	t	2	1
587	1	16827	2013-03-31 00:17:34	t	1	1
588	1	0	2013-03-31 00:17:34	t	1	2
589	2	5705	2013-03-31 00:17:34	t	1	1
590	2	28145	2013-03-31 00:17:34	t	3	1
591	2	23281	2013-03-31 00:17:34	t	3	1
592	3	9961	2013-03-31 00:17:34	t	2	1
593	1	4827	2013-03-31 00:17:42	t	1	1
594	1	0	2013-03-31 00:17:42	t	1	2
595	2	491	2013-03-31 00:17:42	t	1	1
596	2	2995	2013-03-31 00:17:42	t	3	1
597	2	11942	2013-03-31 00:17:42	t	3	1
598	3	5436	2013-03-31 00:17:42	t	2	1
599	1	153	2013-03-31 00:17:49	t	1	1
600	1	0	2013-03-31 00:17:49	t	1	2
601	2	32391	2013-03-31 00:17:49	t	1	1
602	2	14604	2013-03-31 00:17:49	t	3	1
603	2	3902	2013-03-31 00:17:49	t	3	1
604	3	292	2013-03-31 00:17:49	t	2	1
605	1	19718	2013-03-31 00:17:57	t	1	1
606	1	0	2013-03-31 00:17:57	t	1	2
607	2	12382	2013-03-31 00:17:57	t	1	1
608	2	17421	2013-03-31 00:17:57	t	3	1
609	2	18716	2013-03-31 00:17:57	t	3	1
610	3	19895	2013-03-31 00:17:57	t	2	1
611	1	11538	2013-03-31 00:18:04	t	1	1
612	1	0	2013-03-31 00:18:04	t	1	2
613	2	5447	2013-03-31 00:18:04	t	1	1
614	2	21726	2013-03-31 00:18:04	t	3	1
615	2	14771	2013-03-31 00:18:04	t	3	1
616	3	1869	2013-03-31 00:18:04	t	2	1
617	1	17035	2013-03-31 00:18:12	t	1	1
618	1	0	2013-03-31 00:18:12	t	1	2
619	2	19912	2013-03-31 00:18:12	t	1	1
620	2	25667	2013-03-31 00:18:12	t	3	1
621	2	26299	2013-03-31 00:18:12	t	3	1
622	3	9894	2013-03-31 00:18:12	t	2	1
623	1	30333	2013-03-31 00:18:19	t	1	1
624	1	0	2013-03-31 00:18:19	t	1	2
625	2	28703	2013-03-31 00:18:19	t	1	1
626	2	23811	2013-03-31 00:18:19	t	3	1
627	2	31322	2013-03-31 00:18:19	t	3	1
628	3	17673	2013-03-31 00:18:19	t	2	1
629	1	28253	2013-03-31 00:18:27	t	1	1
630	1	0	2013-03-31 00:18:27	t	1	2
631	2	4664	2013-03-31 00:18:27	t	1	1
632	2	15141	2013-03-31 00:18:27	t	3	1
633	2	7711	2013-03-31 00:18:27	t	3	1
634	3	6868	2013-03-31 00:18:27	t	2	1
635	1	32757	2013-03-31 00:19:08	t	1	1
636	1	0	2013-03-31 00:19:08	t	1	2
637	2	25547	2013-03-31 00:19:08	t	1	1
638	2	27644	2013-03-31 00:19:08	t	3	1
639	2	32662	2013-03-31 00:19:08	t	3	1
640	3	20037	2013-03-31 00:19:08	t	2	1
641	1	27529	2013-03-31 00:19:15	t	1	1
642	1	0	2013-03-31 00:19:15	t	1	2
643	2	12859	2013-03-31 00:19:15	t	1	1
644	2	8723	2013-03-31 00:19:15	t	3	1
645	2	9741	2013-03-31 00:19:15	t	3	1
646	3	778	2013-03-31 00:19:15	t	2	1
647	1	1842	2013-03-31 00:19:42	t	1	1
648	1	0	2013-03-31 00:19:42	t	1	2
649	2	12316	2013-03-31 00:19:42	t	1	1
650	2	3035	2013-03-31 00:19:42	t	3	1
651	2	22190	2013-03-31 00:19:42	t	3	1
652	3	288	2013-03-31 00:19:42	t	2	1
\.


--
-- TOC entry 1956 (class 0 OID 0)
-- Dependencies: 170
-- Name: values_pkid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('values_pkid_seq', 652, true);


--
-- TOC entry 1932 (class 2606 OID 32779)
-- Name: meters_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY meters
    ADD CONSTRAINT meters_pkey PRIMARY KEY (pkid);


--
-- TOC entry 1936 (class 2606 OID 32801)
-- Name: param_kinds_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY param_kinds
    ADD CONSTRAINT param_kinds_pkey PRIMARY KEY (pkid);


--
-- TOC entry 1934 (class 2606 OID 32793)
-- Name: values_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "values"
    ADD CONSTRAINT values_pkey PRIMARY KEY (pkid);


--
-- TOC entry 1949 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2013-03-31 12:37:20

--
-- PostgreSQL database dump complete
--

