
## Repeater
#
# goal: Repeat tasks by given interval basis
#
class Repeater
  include Celluloid

  # = repeat_every
  #
  # Execute action on specific time interval (in seconds)
  #
  # == Parms:
  #  - interval - in seconds to execute next action
  #  - kind - :start_now for first execute, then wait, :start_later - for waiting first
  #  - &block (implicit) - to execute
  #
  def repeat_every(interval, kind)

    raise "Unable to work without block" unless block_given?

    loop do

      case kind
      when :start_now

        yield

        sleep(interval)

      when :start_later

        sleep(interval) 

        yield

      else

        raise "Unknown kind for repeat #{kind}" 
      end
    end
  end
end
