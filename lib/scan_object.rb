
class ScanObject

	attr :caption
	attr :id
	attr :cfg

	def initialize( aid, acaption, acfg )

		@cfg = acfg
		@id = aid
		@caption = acaption

	end


	def get_params

		@cfg[:params].select do|cfg_parm| 

			parm_object = cfg_parm[:object]

			debug_info "SCAN OBJ:: get params for #{cfg_parm}"

		  find_object_id_for( parm_object )	== id

		end
		.map{|parm| parm[:caption]}

	end


	def to_s

		"#<ScanObject: #{caption}, #{id}>"

	end

	private #
	######### AREA


	def find_object_id_for( obj )

		return nil if @cfg.nil?
		return nil if @cfg[:objects].nil?
		return nil if @cfg[:objects][obj].nil?

		id = @cfg[:objects][obj][:id]

		debug_info "SCAN OBJ:: #{obj} has #{id}" if id

		raise "SCAN DATA:: object id for #{obj} not found in config" if id.nil?

		id
	end


end
