
# = DataRetrieveManager
#
# Data scaner and db saving  manager
#
class DataRetrieveManager

  include Celluloid


  attr :scanning_data
  attr :scanning_arc

  alias :scanning_data? :scanning_data
  alias :scanning_arc? :scanning_arc

  attr :old_val_time, :old_arc_time

	attr :current_object

  attr_accessor :cfg


  def initialize

    @old_val_time = Time.now
    @old_arc_time = Time.now

    @scanning_data = false
    @scanning_arc = false
		@current_object = 0
  end



  def scan_data

    return if @scanning_data

		Actor[:message_board].mailbox << :scan_started

    @scanning_data = true

    @scan_cmd ||= ScanDataCommand.new( cfg )

    @scan_cmd.execute do |cmd|

			@current_object = cmd.current_object_no

			debug_info "DRM:: curr #{@current_object}"

		end

    Actor[:message_board].mailbox << :scan_complete
  ensure

    @scanning_data = false
		@old_val_time = Time.now
  end


  #todo: extract strategies of scanning
  def scan_archives

    return if @scanning_arc

		Actor[:message_board].mailbox << :scan_arc_started

    @scanning_arc = true

    @scan_arc_cmd ||= ScanArchivesCommand.new

    @scan_arc_cmd.cfg = cfg

    @scan_arc_cmd.execute

    Actor[:message_board].mailbox << :scan_arc_complete
  ensure

    @scanning_arc = false
		@old_arc_time = Time.now
  end


  def scanning?

    @scanning_data || @scanning_arc
  end


  private 
  ######## SECTION



end
