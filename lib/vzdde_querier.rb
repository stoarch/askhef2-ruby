
class EVzddeError < StandardError 

  attr :dde_error

  def initialize( dde_err )

    @dde_error = dde_err
  end
end

# = This class query vzljot dde values and return string of res
#
# == Info
#   Usage of 'Spserver' is written in documentation
#
class VzddeQuerier

  WAIT_TIMEOUT = 1 #sec 



	attr_accessor :config

	def initialize
		@srvcalls = []
	end


  try_count = 3


  def query_batch( group, *parms )

    debug_info "vzdde:: query batch : #{group}, #{parms}"

    quoted_parms = parms.map{|p| %Q^"#{p}"^}

		dde_scaner = @config[:scaners][:dde]

    vzdde_parms = "#{dde_scaner} SpServer #{group} #{quoted_parms.join(' ')}"

    @lines = []

    debug_info vzdde_parms

		IO.popen( vzdde_parms ) do |io|
			@lines << io.readlines
		end

    File.open('vzdde.test', 'w'){|f| f << @lines}


    debug_info "VZDDE:: complete query batch"

    # vzdde returns results as strings. we need to skip 1st line - it's logo of 
    #   vzdde. Next we need to skip each first item of returned string. It's only
    #   contains vzdde query progress indicator (Query 1 item).

		result = @lines.flatten![1..-2].each_slice(2).to_a.map{|i| i[1]}


    result
  end



	def query( group, parm_capt )

    raise "Not implemented yet"

	end
end
