
class OpcParm

	attr_accessor :caption  
	attr_accessor :value  
	attr_accessor :is_good  
	attr_accessor :date_query  

	attr_accessor :obj_id, :chanel_id, :parm_kind

	def == (another)
		another.caption == caption and another.value == value and another.is_good == is_good and another.date_query == date_query
	end

	def to_s
		"#<OpcParm caption:#{caption}   value:#{value}   is_good: #{is_good}   date_query: #{date_query.strftime('%d-%m-%Y %H:%M')}"
	end

end

