# Web Service for mnemoscheme (hef2)
#
# Load and display mnemoschemes and graphs, tables and meters
#	in pretty visual way
#
# Copyright (c) 2013 by Afonin Vladimir (mailto:sortie@yandex.ru)
#
# == Functions
#
#		* Load last data
#		* Display table scheme
#		$ Show tab with currently selected meter info
#		+ Show params 
#		+ Show graph for parameter
#		+ Show table for parameter (for period)
#	  + Display mnemoscheme
#		+ Export data to pdf
#		+ Export data to xls
#

require 'sinatra'
require 'sequel'
require 'yamler'
require 'awesome_print'
require 'logger'

$logger = Logger.new( 'mshef-ws.rb', 'daily' )

get '/' do
	@data = receive_last_obj_data()

	erb :ms_table
end


get '/meter/:id' do

	id = params["id"].to_i

	@meter = get_meter( id )
	@obj ||= {}
	@obj[@meter] ||= {}
	@obj[@meter][:data] = receive_last_meter_data( id )

	erb :meter_stat
end


# 
# Receive data from db, group by object/chanel
#
def receive_last_obj_data
	@db ||= connect_to_db()

	data = []
	@obj ||= {}

	@cfg[:objects].each do |ob|

		@obj[ob] ||= {}


		@obj[ob][:data] = receive_last_meter_data( ob[1][:id] )

	end

end


#
# Receive last meter data for meter id
#
def receive_last_meter_data( meter_id )

		@db ||= connect_to_db()


		ob = get_meter( meter_id )

		@obj ||= {}
		@obj[ob] ||= {}

		@obj[ob][:chanels] ||= @db[:values].where(meter_id: meter_id).distinct.select(:chanel).all.map{|c| c.to_a.flatten![1]}


		@obj[ob][:parm_kinds] ||= @db[:values].where(meter_id: meter_id).distinct.select(:parm_kind).all.map{|pk| pk.to_a.flatten![1]}



		ob_ids = @db[:values].where(meter_id: meter_id)
			.where(chanel: @obj[ob][:chanels])
			.where(parm_kind: @obj[ob][:parm_kinds])
			.select(:meter_id, :chanel, :parm_kind)
			.distinct
			.all


		ob_data = [] 

		ob_ids.each do |oid|

			ob_val = @db[:values]
				.where( meter_id: oid[:meter_id] )
				.where( chanel: oid[:chanel] )
				.where( parm_kind: oid[:parm_kind] )
				.order( :date_received )
				.last

			ob_val[:caption] = @parm_captions[[oid[:meter_id],oid[:chanel],oid[:parm_kind]]][:display_text] 


			ob_data <<  ob_val  
		end


		ob_data
end


def get_meter( meter_id )

		@cfg ||= load_cfg 

		@meters ||= {}
		ob = @meters[meter_id]


		if ob.nil?
			ob = @cfg[:objects].detect{|d| d[1][:id] == meter_id}
			@meters[meter_id] = ob
		end

		ob
end

def connect_to_db
	load_cfg()

	Sequel.connect(
		@cfg[:db][:connstr]
	)
end


def load_cfg()

	@cfg = YAML.load_file("./cfg/scaner.cfg.yaml")

	#parse and prepare params captions by their indexes
	@parm_captions = {}

	@cfg[:params].each do |parm|

		parm_key = [
								@cfg[:objects][parm[:object]][:id],
								parm[:chanel],
								@cfg[:param_kinds][parm[:parm_kind]][:id]
		]


		@parm_captions[parm_key] = parm

	end

	@cfg
end
