
        const WIDTH = 300;
        const HEIGHT = 300;
        const CENTER_WIDTH = 30;
        const CENTER_HEIGHT = 30;

        const RADIUS = 150;
        const CENTER_RADIUS = 70;

        const MAJOR_TICK_LEN = 30;
        const MINOR_TICK_LEN = 15;
        const MICRO_TICK_LEN = 8;

        const DEGREE = Math.PI/180;
        const DEGREES_30 = 30*DEGREE;
        const DEGREES_60 = 60*DEGREE; 

        const COUNT_MAJOR_TICKS = 8;
        const COUNT_MINOR_TICKS = 7;
        const COUNT_MICRO_TICKS = 60;

        const MAJOR_TICKS_ANGLE = 275/COUNT_MAJOR_TICKS*DEGREE;
        const MINOR_TICKS_ANGLE = MAJOR_TICKS_ANGLE;

        const ARROW_HEAD_LEN = 35;
        const ARROW_SHIFT_LEN = 50;

        const MAX_PRESSURE = 7;
        const MIN_PRESSURE = 0;

        const SECOND = 1000;
        const MINUTE = 60*SECOND;
        const MINUTES = 60*SECOND;

        const FILLED = true;

        window.Meters = {};

Meters.PressureMeter = function(type){
  this.type = type;
  this.point_value = 2.34;
  this.time = "12:30";
  this.point_caption = "Нет";
  this.min_val = 2;
  this.max_val = 5;

  this.start_angle = 150*DEGREE;
  this.shift_angle = 34.4*DEGREE;

  this.address = 0;
  this.blink_filled = false;
  this.canvas = null;

  this.draw = function(){

        this.context = this.canvas.getContext('2d');

        this.draw_outer_circle();

        this.draw_min_gauge();
        this.draw_normal_gauge();
        this.draw_red_gauge();

        this.draw_outer_shadow_circle();

        this.show_major_ticks();
        this.show_minor_ticks();

        this.draw_internal_value_circle();

        this.draw_ticks_values();
        this.show_arrow();
      }



      this.draw_outer_circle = function(){

        this.context.lineWidth = 2;
        this.context.strokeStyle = 'black';
        this.context.fillStyle = "aqua";

        this.context.beginPath();
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - 5, 0, 2*Math.PI, false);
        this.context.fill();
        this.context.stroke();
      }



      this.draw_min_gauge = function(){

        this.context.shadowColor = "none";
        this.context.shadowBlur = 0;
        this.context.shadowOffsetX = 0;
        this.context.shadowOffsetY = 0;


        var max_angle = this.start_angle + this.shift_angle * this.min_val;
        var min_angle = this.start_angle;

        this.context.strokeStyle = "#FFD175";
        this.context.fillStyle = "lightblue";
        this.context.beginPath();
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - 8, min_angle, max_angle, false );
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - MAJOR_TICK_LEN, max_angle, min_angle, true );


        this.context.stroke();
        this.context.fill();
      }



      this.draw_normal_gauge = function(){

        var min_angle = this.start_angle + this.shift_angle * this.min_val;
        var max_angle = min_angle + this.shift_angle * (this.max_val - this.min_val);

        this.context.strokeStyle = "green";
        this.context.fillStyle = "#00FF00";
        this.context.beginPath();
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - 8, min_angle, max_angle, false );
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - MAJOR_TICK_LEN, max_angle, min_angle, true );


        this.context.stroke();
        this.context.fill();

        this.context.strokeStyle = "black";
      }



      this.draw_red_gauge = function(){

        var min_angle = this.start_angle + this.shift_angle * this.max_val;
        var max_angle = min_angle + this.shift_angle * (MAX_PRESSURE - this.max_val);

        this.context.strokeStyle = "red";
        this.context.fillStyle = "#E00000";
        this.context.beginPath();
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - 8, min_angle, max_angle, false );
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - MAJOR_TICK_LEN, max_angle, min_angle, true );


        this.context.stroke();
        this.context.fill();

        this.context.strokeStyle = "black";
      }



      this.draw_outer_shadow_circle = function (){

        this.context.lineWidth = 2;
        this.context.strokeStyle = 'black';

        this.context.shadowColor = "#5D4D4E";
        this.context.shadowBlur = 5;
        this.context.shadowOffsetX = 5;
        this.context.shadowOffsetY = 5;

        this.context.beginPath();
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - 5, 0, 2*Math.PI, false);
        this.context.stroke();
      }



      this.show_major_ticks = function(){

        this.context.shadowColor = "#5D4D4E";
        this.context.shadowBlur = 5;
        this.context.shadowOffsetX = 5;
        this.context.shadowOffsetY = 5;

        this.context.translate( WIDTH/2, HEIGHT/2 );

        this.context.rotate( DEGREES_60 );

        this.context.lineWidth = 3;
        for( var i = 0; i < COUNT_MAJOR_TICKS; i++ ){

          this.context.beginPath();
          this.context.moveTo( 0, RADIUS - 5 );
          this.context.lineTo( 0, RADIUS - MAJOR_TICK_LEN );
          this.context.stroke();
          this.context.fill();

          this.context.rotate( MAJOR_TICKS_ANGLE );
        }

        this.context.setTransform(1,0,0,1,0,0);
      }



      this.show_minor_ticks = function(){

        this.context.translate( WIDTH/2, HEIGHT/2 );

        this.context.rotate( 77*DEGREE );

        this.context.lineWidth = 3;
        for( var i = 0; i < COUNT_MINOR_TICKS; i++ ){

          this.context.beginPath();
          this.context.moveTo( 0, RADIUS - 5 );
          this.context.lineTo( 0, RADIUS - MINOR_TICK_LEN );
          this.context.stroke();
          this.context.fill();

          this.context.rotate( MINOR_TICKS_ANGLE );
        }

        this.context.setTransform(1,0,0,1,0,0);
      }



      this.draw_internal_value_circle = function(){

        this.context.lineWidth = 1.5;

        var circle_color = 'lightgreen';

        if( !this.blink_filled ){

          if( this.point_value < this.min_val ){
            circle_color = 'yellow';
          }else if( this.point_value > this.max_val ){
            circle_color = 'red';
          }
        }

        this.context.fillStyle = circle_color;
        this.context.beginPath();
        this.context.arc(WIDTH/2, HEIGHT/2, CENTER_RADIUS, 0, 2*Math.PI, false);
        this.context.stroke();
        this.context.fill();

        this.show_value();
      }



      this.has_alert = function() {

        var alert_state = (this.point_value < this.min_val)||(this.point_value > this.max_val);

        if( !alert_state )
          this.blink_filled = alert_state;

        return alert_state;
      }


      this.clear = function(){
        this.context.setTransform(1,0,0,1,0,0);
        this.context.clearRect( 0, 0, WIDTH, HEIGHT + 20 );
      }


      this.blink = function(){
        this.blink_filled = !this.blink_filled;
        this.redraw();
      }


      this.redraw = function(){
        this.clear();
        this.draw();
      }



      this.show_value = function(){

        this.context.shadowColor = "none";
        this.context.shadowBlur = 0;
        this.context.shadowOffsetX = 0;
        this.context.shadowOffsetY = 0;

        this.context.fillStyle = "#000000";
        this.context.textAlign = 'center';
        this.context.font = "68px Arial";
        this.context.fillText(this.point_value,WIDTH/2, HEIGHT/2 + 23);

        this.context.font = "30px Arial";
        this.context.fillText(this.point_caption, WIDTH/2, HEIGHT + 20 );

        this.context.font = "35px Arial";
        this.context.fillText(this.time, WIDTH/2, HEIGHT/2 + 117 );

        this.context.textAlign = 'left';
      }



      this.draw_ticks_values = function(){

        this.context.font = "30px Arial";
        this.context.fillText("0", 50, 215 );
        this.context.fillText("1", 33, 153 );
        this.context.fillText("2", 60, 95 );
        this.context.fillText("3", 110, 60 );
        this.context.fillText("4", 173, 60 );
        this.context.fillText("5", 227, 98 );
        this.context.fillText("6", 250, 153 );
        this.context.fillText("7", 240, 215 );
      }



      this.show_arrow = function(){

        var arrow_color = 'green';

        if( this.point_value < this.min_val ){
          arrow_color = 'yellow';
        }else if( this.point_value > this.max_val ){
          arrow_color = 'red';
        }

        this.context.fillStyle = arrow_color; //'#5ce68a';
        this.context.strokeStyle = 'black';
        this.context.globalAlpha = 0.65;
        this.context.lineWidth = 3;

        this.context.shadowColor = "#5D4D4E";
        this.context.shadowBlur = 5;
        this.context.shadowOffsetX = 5;
        this.context.shadowOffsetY = 5;


        this.point_value = Math.min(7, this.point_value);
        this.point_value = Math.max(0, this.point_value);

        angle = get_angle_for( this.point_value );

        this.context.translate( WIDTH/2, HEIGHT/2 );
        this.context.rotate( angle ); 
        this.context.translate( ARROW_SHIFT_LEN, ARROW_SHIFT_LEN );
        this.context.beginPath();
        this.context.lineTo( ARROW_HEAD_LEN, ARROW_HEAD_LEN );
        this.context.lineTo( 0, ARROW_HEAD_LEN/2 );
        this.context.lineTo( ARROW_HEAD_LEN/2, 0 );
        this.context.closePath();
        this.context.fill();
        this.context.stroke();
      }



      function get_angle_for( value ){

        return value*34.4*DEGREE + 105*DEGREE;
      }
};
