# encoding: utf-8
# This class provide asynchronous writing of archives to db
#
# == Info
#  Underlying db is specified by config
#
class DbArcWriter

    include Celluloid

    # config file for db access
    attr_accessor :cfg

    # archive for store
    attr_accessor :arc

    # true when saving in progress
    attr :is_saving

    # db backend, that can be configured externally
    attr_accessor :db

    def execute

			raise "DbArcWriter:: No db specified" if @db.nil?

        @is_saving = true

        debug_info "DbArcWriter:: Start archive #{arc.caption} saving..."


        prepare_values



        @db.transaction do
            
            save_arc_info

            save_arc_header

            save_rows

        end


        debug_info "DbArcWriter:: Saving done"

    rescue Exception => ex

        ap "Error: #{ex}"
        $logger.error ex
    ensure

       @is_saving = false

    end


    private

    def save_arc_info

      debug_info "DbArcWriter:: Save archive info..."

      value = @db[:archives].where( name: @arc.caption ).first

      debug_info "DbArcWriter:: archive does not found" unless value

      @db[:archives].insert( name: @arc.caption ) if ( value == nil ) 

      value = @db[:archives].where( name: @arc.caption ).first

      unless value.nil?

        @arc.id = value[:id] #todo: make this side effect visible outside

      end

      raise "DbArcWriter:: Unable to get id for archive" if ( @arc.id == 0 ) || ( @arc.id.nil? )

      debug_info "DbArcWriter:: Done" 

    end


		ARC_ID_FIELD = 1
		CAPTION_FIELD = 0


    def select_stored_achive_cols

      # structure of array item is [[ [:id,1],[:arc_id, 1], [:caption, 'demo'] ]]

			@existent_cols = {}
			
			@db[:arc_cols].where(arc_id: @arc.id).all.map{|v| @existent_cols[v[:caption]] = v[:id] } 

      debug_info "DbArcWriter:: existent cols: #{@existent_cols[0..10]}"

			@existent_cols
    end


    def store_new_columns

			# each of these hashes must be arrayed
			@new_cols = @cur_cols.to_a.map{|v| v[CAPTION_FIELD]} - @existent_cols.to_a.map{|v| v[CAPTION_FIELD]} 

			cur_date = DateTime.now.strftime('%y/%m/%d %H:%M')

			if @new_cols

				@new_cols.each do |nc|

          insert_sql = "insert into arc_cols (arc_id, caption, date_write) values ( #{@arc.id}, '#{nc}', '#{cur_date}')" 


          @db.run insert_sql				

        end
			end
    end


    def select_current_cols

			@cur_cols = {}

      @arc.header.columns.map{|c| @cur_cols[c.caption] = c.id}

			debug_info "DbArcWriter:: current cols #{@cur_cols[0..10]}"

			@cur_cols
    end


		def update_column_codes

			existed_cols = select_stored_achive_cols

			@arc.header.columns.each do |col|

				col.id = existed_cols[ col.caption ]

			end

		end


    def save_arc_header

      debug_info "DbArcWriter:: Save archive header..."
      
      select_stored_achive_cols

      select_current_cols

      store_new_columns

			update_column_codes

      debug_info "DbArcWriter:: Done"

    end


    def save_rows

      debug_info "DbArcWriter:: Save archive rows..."


      @arc.rows.each do |row|

        row.cells.each do |cell|

          @db[:arc_vals].insert( date_write: row.cur_date, arc_id: @arc.id, row_id: row.id, arc_col_id: cell.column.id, value: cell.value)
        end

      end

      debug_info "DbArcWriter:: Done" 
    end


    def prepare_values

    end


end
