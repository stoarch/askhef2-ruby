#encoding: utf-8

# This program scan the opc server and store it values to db for 
# process later and display mnemoschemes and graphs
#
# Author:: Afonin Vladimir (mailto:stormarchitextor@gmail.com)
# Copyright:: Copyright(c) 2013 Afonin Vladimir
# License:: See license.txt
#
# == Functions:
#
#   * Display UI
#   * Connect to OPC
#   * Read single parm
#   * Read multiple parms
#   * Log events and errors
#   * Display errors
#   * Display clock status
#   * Regular query
#   * Display readen parms
#   * Read config from file
#   * Read data by config
#   * Read data async
#   * Connect to DB
#   * Store data to db (async)
#   * Log db operations
#   * Log db errors
#   * Display saving status
#   * Display save result
#   * Display animations (process)
#   ~ Scan errors/alarms
#		* Scan archives
#		* Read Vzsp configuration
#		* Read Vzsp with DDE
#		* Save archives
#   ~ Save alarms
#		$ Regular scanning for archives
#		$ Log archive operations
#		$ Display archive scan status
#   ~ Display alarms scan status
#   ~ Display color animation of currently scanned object (with shaders)
#   $ Display animations (progress for objects)
#		~ Display animations (for each text)
#		~ Display scrollable animation of params of object
#   ~ Scroll console 
#   ~ Gradient console
#   ~ Console scrollbars and navigation
#   ~ Scan from multiple OPC
#   ~ Store to multiple DB

require 'logger'
require 'yamler'
require 'ut'
require 'ostruct'
require 'celluloid/autostart'
require 'celluloid'
require 'awesome_print'
require 'sequel'
require 'win32ole'
require 'timeout'

require_relative 'archive'
require_relative 'db_arc_writer'
require_relative 'dbwriter'
require_relative 'scaner'
require_relative 'message_board'
require_relative 'progress_view'
require_relative 'screen_view'
require_relative 'repeater'
require_relative 'data_retrieve_manager'
require_relative 'math_tools'
require_relative 'vzsettings_reader'
require_relative 'access_db'
require_relative 'scaner_arc_data'

require_relative 'scan_data_command'
require_relative 'scan_archive_command'
require_relative 'kernel_ext'
require_relative 'arc_scan_criteria'
require_relative 'device'

require_relative 'scan_object'
require_relative 'scan_object_list'


$SHOW_DEBUG_INFO = true
$LOG_DEBUG_INFO = true

## ......... ##
#  CONSTANTS  #
###############

SCAN_INTERVAL = 6 #sec

EPS  = 1e-1

TILE_SIZE = 16

SCALE_X = 1.85
SCALE_Y = 1.25
WINDOW_WIDTH = 640 
WINDOW_HEIGHT = 480 

VIEWPORT_WIDTH = WINDOW_WIDTH/TILE_SIZE
VIEWPORT_HEIGHT = WINDOW_HEIGHT/TILE_SIZE

PARM_LINE = 0

PARM_VAL_CAPTION = 0
PARM_VAL_VALUE = 1
PARM_VAL_GOOD = 2
PARM_VAL_DATEQUERY = 3
PARM_VAL_TIMEQUERY = 4

TIME_RANGE = 11..-1

ARC_TIMER_LEFT = 0
ARC_TIMER_TOP = 13

TIMER_LEFT = 0
TIMER_TOP = 12

YEAR_RNG = 6..9
MONTH_RNG = 3..4
DAY_RNG = 0..1

HOUR_RNG = 0..1
MINUTE_RNG = 3..4


PARM_QUEUE_LINE = 19
PARM_QUEUE_HEIGHT = 4


ERROR_LINE = 25

SCAN_ARC_STATUS_LINE = 16
SCAN_ARC_COMPLETE_LINE = 17

SCAN_STATUS_LINE = 14
SCAN_COMPLETE_LINE = 15

SAVE_STATUS_LINE = 14
SAVE_COMPLETE_LINE = 15
SAVE_STATUS_COL = 20

OBJ_COL_SIZE = 15
OBJ_COL_COUNT = 2
OBJ_LINE_COUNT = 5

OBJ_LIST_X = 0
OBJ_LIST_Y = 4
OBJ_LIST_COL_SHIFT = 2

## ....... ##
#  CLASSES  #
#############



class NilClass

  def scan_complete?

    debug_info "Nil:: scan_complete"

    true

  end

  def scan_arc_complete?

    debug_info "Nil:: scan_arc_complete"

    true

  end

end




## Window
#
# goal: Display main window with GUI
#
# == Functions
#  init ui:: Make gosu window and display it on screen
#  display query time:: Tell to user when next query occurs
#  scan data:: Scan data from server and show result
#  store data:: Connect to db and store data to it
#
class Window < Gosu::Window


  attr_accessor :viewport

  attr :scaner

  def initialize width, height

    super width, height, false

    caption = "ASKUHE2 Data Scaner - Afonin & Co"
    read_config

    @object_capts =   @cfg[:objects].map{|ob| ob[1][:caption]}    

    @mboard = MessageBoard.new
    Celluloid::Actor[:message_board] = @mboard

    @scaner = Scaner.new
    @scaner.link @mboard
    Celluloid::Actor[:scaner] = @scaner

    @db_writer = DBWriter.new
    @db_writer.cfg = @cfg
    Celluloid::Actor[:db_writer] = @db_writer


    @value_scaner = DataRetrieveManager.new
    @value_scaner.cfg = @cfg
    Celluloid::Actor[:data_retrieve_mgr] = @value_scaner


    @progress_anim = Gosu::Image.load_tiles( self, File.dirname(__FILE__) + "/images/loading_41445.png", 50, 50, false)
    @progress_view = ProgressView.new( @progress_anim, WINDOW_WIDTH - 50, WINDOW_HEIGHT - 50)


    @value_scan_interval = @timeout = @cfg[:scaner][:timeout].to_i
    @arc_scan_interval = @arc_scan_timeout = @cfg[:scaner][:arc_timeout].to_i

    start_timers
  end

  

  def start_timers

    @value_scan_repeater ||= Repeater.new

    @value_scan_repeater.async.repeat_every( @value_scan_interval, :start_later ) do 

      retrieve_mgr.async.scan_data

      wait_scan_data_completion
    end

    @arc_scan_repeater ||= Repeater.new

    @arc_scan_repeater.async.repeat_every( @arc_scan_interval, :start_later ) do
      
      retrieve_mgr.async.scan_archives

      wait_scan_arc_completion
    end
  end


  def wait_scan_arc_completion

    begin

      sleep 0.5 

    end until message_board.scan_arc_complete?

		debug_info "Window:: Archive scan completed"

  end


  def wait_scan_data_completion 

    begin

      sleep 1

    end until message_board.scan_complete?

		debug_info "Window:: Data scan completed"

  end


  def message_board

    Celluloid::Actor[:message_board] if Celluloid::Actor[:message_board].alive?

  end


  def button_down( id )

    close if id == Gosu::KbEscape
  end


  def draw

    draw_scan_status
    draw_arc_scan_status

    draw_save_status

    draw_object_list

    draw_error_message if @mboard.has_new_error?

    $viewport.draw

    @progress_view.draw if retrieve_mgr.scanning? or @db_writer.is_saving? 
  end


  def retrieve_mgr 

    Celluloid::Actor[:data_retrieve_mgr]
  end


  def update

    update_clock( @timeout ) unless retrieve_mgr.scanning_data?   

    update_arc_clock( @arc_scan_timeout ) unless retrieve_mgr.scanning_arc? 
  end


  # ....... #
  # PRIVATE #
  ###########
  private


  def draw_error_message

		err_msg = @mboard.error_message

		msg = 
			if err_msg.class == Array
				err_msg.map{|m| m.strip.chomp}.join('; ')  
				elsif err_msg.class == String
					err_msg.strip.chomp 
				else
					ap err_msg.inspect
					ap err_msg.class

					if err_msg.respond_to? :message
						err_msg.message
					else
						"Unknown error msg class #{err_msg}"
					end
				end

    $viewport.show_error_msg msg
  end


  def draw_object_list

    @object_capts ||=   @cfg[:objects].map{|ob| ob[1][:caption]}    

    $viewport.draw_list( @object_capts, OBJ_LIST_X, OBJ_LIST_Y, OBJ_LINE_COUNT, OBJ_COL_COUNT, retrieve_mgr.current_object)

  end



  def read_config
    @config = @cfg = YAML.load_file(File.dirname(__FILE__) + "/cfg/scaner.cfg.yaml")

    ENV['OPC_SERVER'] = @cfg[:opc_server]
    ENV['OPC_CLASS'] = @cfg[:opc_class]
  end


  def update_arc_clock( arc_timeout )
		
		timeout = arc_timeout - (Time.now - retrieve_mgr.old_arc_time).round(0)

    $viewport.show_arc_timer_status( timeout ) if timeout >= 0

		$viewport.show_error_msg "Timeout for archive is broken: #{timeout}" if timeout < 0
  end


  def update_clock( timeout )

		data_timeout = timeout - (Time.now - retrieve_mgr.old_val_time).round(0)

    $viewport.show_timer_status( data_timeout ) if data_timeout >= 0

		$viewport.show_error_msg "Timeout for data is broken: #{data_timeout}" if data_timeout < 0

  end


  def draw_arc_scan_status

    if retrieve_mgr.scanning_arc? 

      $viewport.show_arc_scan_status
    else

      $viewport.hide_arc_scan_status
    end
  end


  def draw_scan_status

    if retrieve_mgr.scanning_data?

      $viewport.show_scan_status
    else

      $viewport.hide_scan_status
    end
  end


  def draw_save_status

    if @db_writer.is_saving

      $viewport.show_save_status
    else

      $viewport.show_save_end_status
    end
  end
end



LOGOUT ||= 'ash2-server.log'


COUNT_OF_LOGS = 3
LOG_MAX_SIZE = 1_048_576 #bytes: 1MiB
## ......... ##
#  MAIN CODE  #
###############

$logger = Logger.new LOGOUT, COUNT_OF_LOGS, LOG_MAX_SIZE 
$logger.level = Logger::DEBUG

$logger.info 'Server started...'

$window = Window.new( WINDOW_WIDTH, WINDOW_HEIGHT )


@renderer = UT::FontRenderer.new font_name: 'fonts/DejaVuSansMono.ttf', tile_size: TILE_SIZE

$viewport = ScreenView.new renderer: @renderer, width: VIEWPORT_WIDTH, height: VIEWPORT_HEIGHT




## DISPLAY TITLE ##

$viewport.put_string 0, 0, "ASKU HEF 2 - Almaty", Gosu::Color::WHITE
$viewport.put_string 0, 1, "Data Scaner server \xD8\xB9", Gosu::Color::CYAN
$viewport.put_string 0, 2, "Copyright (c) 2013 by Afonin Vladimir", Gosu::Color::GRAY

## INIT WINDOW ##

$window.viewport =  $viewport


## RUN ##
$window.show unless $testing

$logger.info 'Server stopped.'
