module Kernel

  MAX_MESSAGE_LEN = 70

  def debug_info message


			ap message[0..MAX_MESSAGE_LEN]


			$logger.info "\n---------------------------------------------\n"

			$logger.debug message
  end


  def debug_error message

    ap message[0..MAX_MESSAGE_LEN]

    $logger.debug "\n##############################################\n"
    $logger.error message
    $logger.debug $!
  end
end


