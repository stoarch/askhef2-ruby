# encoding: utf-8

## MessageBoard
#
# goal: Store the messages from scaners and storers for future use of main window
#
class MessageBoard 
  include Celluloid

  attr :has_new_arc
  attr :has_new_data
  attr :has_new_error

  attr :scan_started
  attr :scan_complete

  attr :scan_arc_started
  attr :scan_arc_complete

  attr :has_new_scan_error

  alias :has_new_arc? :has_new_arc
  alias :has_new_data? :has_new_data
  alias :has_new_error? :has_new_error

  alias :scan_started? :scan_started
  alias :scan_complete? :scan_complete

  alias :scan_arc_started? :scan_arc_started
  alias :scan_arc_complete? :scan_arc_complete

  alias :has_new_scan_error? :has_new_scan_error

  alias :has_data? :has_new_data


  finalizer :echo_finalizer


  def echo_finalizer

    puts "MessageBoard destroyed"
  end


  def actor

    Actor.current
  end


  def initialize

    @has_new_data = false
    @has_new_error = false

    @error_messages = []
    @data_value = []
    @arc_value = []

    async.wait_for_messages
  end


  def wait_for_messages

    loop do

      break unless actor.mailbox.alive?

      @msg_value = receive

      debug_info "MessageBoard:: received #{@msg_value}"


      if @msg_value.class  == Symbol 

        debug_info "MessageBoard:: Symbol message received #{@msg_value}"

        check_symbol_messages @msg_value

      elsif @msg_value.class == ScanerData 

        debug_info "MessageBoard:: Data received"

        @has_new_data = true

        @data_value.push @msg_value.value

        debug_info "MessageBoard:: message #{@msg_value.value.inspect}"
        debug_info "MessageBoard:: data #{@data_value}"
        debug_info "...   size #{@data_value.size}"

      elsif @msg_value.class == ScanerArcData 

        debug_info "MessageBoard:: Archive received"

        @has_new_arc = true

        @arc_value.push @msg_value.value

      elsif ( @msg_value.class == ScanerError ) or ( @msg_value.class == DbWriterError )

        debug_info "MessageBoard:: ScanerError received"

        @has_new_scan_error = true

        @scan_error = @msg_value.message

        set_error @msg_value.message

      else

        debug_info "MessageBoard:: Unknown message received #{@msg_value}"
      end
    end

  rescue Exception => ex

    debug_error "MessageBoard:: Error: #{ex}"
  end


  def check_symbol_messages( value )


        case value 

        when :scan_complete then check_completed_scan 

        when :scan_arc_complete then check_completed_arc_scan

        when :scan_started then check_started_scan

        when :scan_arc_started then check_started_arc_scan

        end
  end


  def check_completed_arc_scan

    @scan_arc_started = false
    @scan_arc_complete = true

  end


  def check_completed_scan

    @scan_started = false
    @scan_complete = true

  end


  def check_started_scan

    @scan_started = true
    @scan_complete = false

  end


  def check_started_arc_scan

    @scan_arc_started = true
    @scan_arc_complete = false

  end



  def value

    res = @data_value.shift

    @has_new_data = @data_value.size > 1

    debug_info "MessageBoard:: res val #{res}"
    debug_info "...   has_new_data: #{@has_new_data}"
    debug_info "...   vals size: #{@data_value.size}"

    res

  end


  def archive

    arc = @arc_value.shift

    @has_new_arc = @arc_value.size > 1

    debug_info "MessageBoard:: archive #{arc}"
    debug_info "...  size #{@arc_value.size}"
    debug_info "...  has new arc: #{@has_new_arc}"

    arc
    
  end


  def set_error message

    debug_info "MessageBoard:: Error is received #{message}"

    @has_new_error = true
    @error_messages ||= []
    @error_messages.push message
  rescue

    ap $!
  end


  def error_message

    @error_messages ||= []
    @has_new_error = false if @error_messages.empty?
    @error_messages.unshift[0]
  end


  def scan_error

    @has_new_scan_error = false
    @scan_error
  end
end
