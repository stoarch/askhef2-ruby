Sequel.migration do
  change do
    create_table(:meters) do
      primary_key :pkid, :type=>Bignum
      Bignum :id, :null=>false
      String :caption, :size=>50, :null=>false
    end
    
    create_table(:param_kinds) do
      primary_key :pkid
      String :caption, :size=>50
    end
    
    create_table(:values) do
      primary_key :pkid, :type=>Bignum
      Bignum :meter_id
      Float :value
      DateTime :date_received
      TrueClass :is_good
      Integer :chanel
      Integer :parm_kind
    end
  end
end
