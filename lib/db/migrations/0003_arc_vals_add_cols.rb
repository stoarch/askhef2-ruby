
Sequel.migration do

  change do

    alter_table :arc_cols do

			add_column :date_write, Date 
    end

    alter_table :arc_vals do

			add_column :date_write, Date 
    end
  end

  
end
