Sequel.migration do

  change do

    create_table :archives do

      primary_key :id
      String :name, null: false
    end

    create_table :arc_cols do

      primary_key :id
      Integer :arc_id
      String :caption
    end

    create_table :arc_vals do

      primary_key :id
      Integer :arc_id
      Integer :arc_col_id
      String  :value
    end
  end

  
end
