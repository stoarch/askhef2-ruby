# encoding: utf-8

require_relative 'scaner_data'
require_relative 'opc_parm'
require_relative 'vzdde_querier'
require_relative 'vzopc_querier'

class EScaner < StandardError
  def initialize( msg="Scaner error" )
    super(msg) 
  end
end


class ScanerError
  attr :message

  def initialize( msg )
    @message = msg
  end
end



# This class scan the opc server and parse result
#
# == Info for OPC
#  At the time of write this code we have no ruby gem for opc server
#       access. So this can be overriden by using OpenOPC python app.
#    We only call them and parse results. Then store it in db
#
class Scaner 

  include Celluloid


  attr_accessor :data_loader
  attr_accessor :arch_loader
  attr_accessor :config

  attr :arc

  def initialize

    @parm = OpenStruct.new
    @parm.caption = 'none'
    @parm.value = 0.0;
    @parm.is_good = false;
    @parm.date_query = DateTime.now

    @data_loader ||= VzopcQuerier.new       

    @arch_loader ||= VzddeQuerier.new

  end


  # = Read multiple params from opc server
  #
  # [parms]
  #       caption_ary:: for each of opc item specified. For command line
  #           compatibility reasons summary length of caption ary must be
  #           less than MAX_CMD_LINE_LEN 
  #
  def read_parms( *caption_ary ) 

    debug_info "SCANER:: read parms #{caption_ary}"
    debug_info "...   size: #{caption_ary.size}"

    @parms = []

    @lines = query_opc_items( caption_ary.flatten )

    parse_result_lines

    raise "SCANER:: Message board is dead! " unless Actor[:message_board].mailbox.alive?

    debug_info "SCANER:: parms #{@parms}"
    debug_info "...   size #{@parms.size}"

    Actor[:message_board].mailbox << ScanerData.new( @parms )

    @parms
  rescue Exception => ex

    debug_error "SCANER:: Error #{ex.message}"
    debug_info $@

    Actor[:message_board].mailbox << ScanerError.new( ex.message ) if Actor[:message_board].alive?
  end



  # = Read archive for vzljot from device
  #
  # = parms:
  #   - device - to query
  #   - criteria - to specify archive records for receival
  #
  def read_archive( device, criteria ) 

     debug_info "SCANER:: Read archive #{device.inspect}, #{criteria.inspect}"

     @arc = 

     case criteria.kind

     when :hourly

       read_hourly_archive( device, criteria )

     when :daily

       read_daily_archive( device, criteria ) 

     when :monthly

       read_monthly_archive( device, criteria )

     end

     Actor[:message_board].mailbox << ScanerArcData.new( @arc ) if Actor[:message_board].mailbox.alive?

     @arc

  rescue Exception => ex

    debug_error "SCANER:: Error #{ex}"

    $logger.ap $@

    Actor[:message_board].mailbox << ScanerError.new( ex.message ) if Actor[:message_board].alive?

  end


  SEPARATOR_CODE = 0xC 

  HEADER_ITEM = 0
  DUMMY_ID = 1

  def read_hourly_archive( device, criteria ) 

    arc_id = DUMMY_ID

    @arc = Archive.new
    @arc.caption = criteria.arc_caption

    date_criteria = []

    # this is header query
    batch_criteria = ["(#{device.id}, #{criteria.arc_code}, 0, 0)"]#header

      date_work = criteria.date_query

    # and here data for this archive
    criteria.count.times do |i|

      date_criteria << DateTime.new( date_work.year, date_work.month, date_work.day, i, 0 )

      batch_criteria << "(#{device.id},#{criteria.arc_code},#{ date_work.day }#{sprintf( '%02d', date_work.month )}#{sprintf( '%02d', i )}00,0,#{criteria.date_query.year},0)"
    end


    read_batch_archive( device, criteria, date_criteria, batch_criteria )
  end


  def read_monthly_archive( device, criteria ) 

    arc_id = DUMMY_ID

    @arc = Archive.new
    @arc.caption = criteria.arc_caption

    # this is header query
    batch_criteria = ["(#{device.id}, #{criteria.arc_code}, 0, 0)"]#header

    date_criteria = []

    date_work = criteria.date_query

    # and here data for this archive
    criteria.count.times do |i|

      date_criteria << DateTime.new( date_work.year, date_work.month - i, date_work.day, 23, 59 )

      batch_criteria << "(#{device.id},#{criteria.arc_code},#{ date_work.day }#{sprintf( '%02d', date_work.month - i )}2359,0,#{criteria.date_query.year},0)"
    end


    read_batch_archive( device, criteria, date_criteria, batch_criteria )
  end




  def read_daily_archive( device, criteria ) 

    date_criteria = []

    # this is header query
    batch_criteria = ["(#{device.id}, #{criteria.arc_code}, 0, 0)"]#header

    # and here data for this archive
    criteria.count.times do |i|

      date_work = criteria.date_query - i

      date_criteria << date_work

      batch_criteria << "(#{device.id},#{criteria.arc_code},#{ date_work.day }#{sprintf( '%02d', date_work.month )}2359,0,#{criteria.date_query.year},0)"
    end

    read_batch_archive( device, criteria, date_criteria, batch_criteria )
  end


  def read_batch_archive( device, criteria, date_criteria, batch_criteria )

    arc_id = DUMMY_ID

    @arc = Archive.new
    @arc.caption = criteria.arc_caption

    raw_arc = 

      begin

        read_raw_archive( device.group, *batch_criteria )

      rescue Exception => ex

        debug_error "SCANER:: Scan raw arc: #{ex}"
        []
      end

    return nil if raw_arc == []


    @arc.header = Archive::Header.make_from( arc_id, extract_header_from( raw_arc[HEADER_ITEM]).unshift( 'date_rec' ) )


    @arc.rows = []
    
    raw_arc[1..-1].each_with_index do |data, row_id| 

      debug_info "SCAN:: data #{data[0..10]}"

      res_data = extract_data_from( data )

      next if res_data.nil?

      @arc.rows << Archive::Row.make_from( arc_id, 
                                          row_id + 1,
                                          date_criteria[row_id],
                                          arc.header.columns,
                                          res_data.unshift( date_criteria[ row_id ] ) ) 
    end



    @arc
  end




  TRY_COUNT = 3
  WAIT_TIMEOUT = 0.1 #s


  def extract_header_from( item )

    item.map{|v| v.strip}[1..-1]
  end


  def extract_data_from( item )

    item.map{|v| v.strip}[3..-1]
  end



  # = Read archives from vzljot
  #
  # Using vzljot dde server to read periodically archives from devices
  #
  # == Parms
  #   - arc_parms - array of params (0 - group id, 1 - query in vzljotsp format (UU,PP,DS,DF[,YS,YF]), 
  #       where UU - device id, 
  #             PP - parm id, 
  #             DS - date start, 
  #             DF - date finish (format: ddmmHHMM), 
  #             YS - year start, 
  #             YF - year finish (format: 2013) 
  #
  def read_raw_archive( group, *arc_parms )

    result = []

    debug_info "SCANER:: Read raw archive #{arc_parms.inspect}"

    @arch_loader.config = config

    @result = 
    if arc_parms.is_a? String

      @arch_loader.query( group, arc_parms ).flatten.split("|\t")

    elsif arc_parms.is_a? Array

      @arch_loader.query_batch( group, *arc_parms ).map{|v| v.split("|\t") }

    end


  rescue Exception => ex

    ex_message = "SCANER:: Error #{ex}"

    debug_error ex_message

    Actor[:message_board].mailbox << ScanerError.new( ex_message ) if Actor[:message_board].alive?

    []
  end


  ## .. PRIVATE .. ##
  private


  def parse_result_lines

    debug_info "SCANER:: received lines #{@lines}"
    debug_info "...   size: #{@lines.size}"

    return [] if @lines.nil? or @lines.empty?

    @lines.each do |line_parms|

      line_parms.each do |line|

        next if line == "\n" 
        next if line =~ /Vzljot|Querying/ # move to VzopcQuerier parsing of vzljot results

          @parm_vals = line.split(/\s{2,}/)

        next if invalid_parm_vals?

        next if @parm_vals[PARM_VAL_VALUE] =~ /None/

          next unless load_parm_from( line )

        @parms << @parm if @parm
      end
    end

    @parms
  end


  def query_opc_items( caption_ary )

    @lines ||= []

    @data_loader.config = config

    caption_ary.each do |parm_capt|

      debug_info "SCANER:: Query parm #{parm_capt}"

      @lines << @data_loader.query( parm_capt ).flatten

      debug_info "... lines:#{@lines}"
    end

    @lines
  end


  def invalid_parm_vals?

    if @parm_vals.nil?

      msg = "SCANER:: Parms is empty for #{line}" 
      
      ap msg
      $logger.error msg
      Actor[:message_board].mailbox << ScanerError.new( msg )
      return true 
    end
    false #all ok
  end



  # = load_parm_from
  #
  # Parse the line to receive parm from it. 
  #
  # == Info
  #   Line must be in style that vzopc show on screen.
  
  def load_parm_from( line )

    return nil if is_invalid_parm_caption?

    @parm = OpcParm.new 

    @parm.caption = @parm_vals[PARM_VAL_CAPTION].rstrip


    if is_invalid_parm_val?

      return nil
    else

      @parm.value = @parm_vals[PARM_VAL_VALUE].gsub(',','.').to_f

    end


    @parm.is_good = @parm_vals[PARM_VAL_GOOD] 


    date_parms = @parm_vals[PARM_VAL_DATEQUERY]

    time_parms = @parm_vals[PARM_VAL_DATEQUERY][TIME_RANGE] #time range

    begin

      @parm.date_query = DateTime.new( date_parms[YEAR_RNG].to_i, date_parms[MONTH_RNG].to_i, date_parms[DAY_RNG].to_i, time_parms[HOUR_RNG].to_i, time_parms[MINUTE_RNG].to_i, 0 ) 

    rescue
      @parm = nil
    end

    @parm 

  rescue Exception => ex

    debug_error  "SCANER:: Error: #{ex}"

    $logger.info $!

    nil 
  end


  def is_invalid_parm_caption?

    return true if @parm_vals.nil? or @parm_vals[PARM_VAL_CAPTION].nil?

    caption = @parm_vals[PARM_VAL_CAPTION].rstrip


    if caption.nil? or caption.strip == ''

      msg = "SCANER:: Error: Unable to work with nil captions." 

      puts msg 
      $logger.error msg 
      Actor[:message_board].mailbox << ScanerError.new( msg )

      return true 
    end

    false   

  rescue Exception => ex

    $logger.error ex
    $logger.info $!

    true  
  end


  def is_invalid_parm_val?

    value = @parm_vals[PARM_VAL_VALUE]

    return true if value.nil? 


    value = value.gsub(',','.')


    if not MathTools.number?(value) 

      msg = "SCANER:: Error: Values must exists" 

      puts msg 
      $logger.error msg
      Actor[:message_board].mailbox << ScanerError.new( msg )

      return true 
    end

    false
  rescue Exception => ex
  
    true
  end

end #Scaner
