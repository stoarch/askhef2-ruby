
class ScanArchivesCommand

	attr_accessor :cfg

	attr :db

	attr :scanning
	attr :old_time

	def initialize

		@current_arc = 0
		@old_time = Time.now
		@scanning = false
	end

	
	def message_board

		Celluloid::Actor[:message_board]

	end


	def execute

		start_scan_arc if @current_arc == 0

		@current_arc ||= 0

		begin


			wait_until_arc_received

			debug_info "SCAN ARC:: has archive #{message_board.has_new_arc?}"

			store_data 

			query_next_arc


			if @current_arc >= @archive_props.size

				debug_info "SCAN ARC:: Complete archive scan"

				@scanning = false
				@old_time = Time.now

			end

			debug_info "SCAN ARC:: #{@current_arc} of #{@archive_props.size}"

		end until @current_arc >= @archive_props.size

		@current_arc = 0
	ensure

		@db.disconnect if @db
		@db = nil

		@scanning = true
		@old_time = Time.now

	end


	private
	######## SECTION

	WAIT_TIMEOUT = 1 #sec


	def wait_until_arc_received

		begin

			sleep WAIT_TIMEOUT

		end until message_board.has_new_arc? 

	end


  def message_board

    Celluloid::Actor[:message_board]

  end


	def store_data

		debug_info "SCAN ARC:: storing archive"

		@arc_writer ||= prepare_arc_writer 

		@arc = message_board.archive

		raise "SCAN ARC:: Error: No archive received for save" if @arc.nil?

		debug_info "SCAN ARC:: Saving archive #{@arc}" 

		@arc_writer.arc = @arc 
		@arc_writer.execute

		debug_info "SCAN ARC:: Archive #{@arc.caption} saved..."

	rescue Exception => ex

		$logger.error ex
		$logger.info $@
	end


	def start_scan_arc

		#todo: implement kernel debug_info
		debug_info  "\n\nSCAN ARC:: Archive scanning started..."


		@scanning = true
		@old_time = Time.now

		@scaner = Celluloid::Actor[:scaner]
		@scaner.config = @cfg

		@settings ||= VzSettingsReader.new( @cfg ) 

		@archive_props = @cfg[:archives]

		scan_current_archive

	rescue Exception => ex

		debug_error ex.inspect

		debug_error $@.inspect

		$viewport.show_error_msg( ex )
	end


	def get_current_device( caption )

		device = Device.new

		device.caption = caption 

		device.id = @settings.group_for( device.caption )

		device
	end


	def get_scan_criteria_for( device, arc_caption, name, count, kind )

		criteria = ArcScanCriteria.new

		criteria.arc_caption = arc_caption 
		criteria.arc_code = @settings.archive_code_for( device.caption, name )
		criteria.name = name
		criteria.date_query = DateTime.now - 1
		criteria.count = count
		criteria.kind = kind


		criteria
	end


	def scan_current_archive

		@archive = nil

		arc_cfg = @archive_props[@current_arc] 

		debug_info "SCAN ARC:: Scan archive: #{arc_cfg[:caption]}"

		device = get_current_device arc_cfg[:object] 

		arc_criteria = get_scan_criteria_for device, arc_cfg[:caption], arc_cfg[:name], arc_cfg[:count], arc_cfg[:kind].to_sym 


		# read archives
		@archive = @scaner.async.read_archive device, arc_criteria
	end


	def query_next_arc

		@current_arc += 1

		return if @current_arc >= @archive_props.size

		debug_info "SCAN ARC:: ARC: #{@archive_props}"

		debug_info "SCAN ARC:: Query archive #{@archive_props[@current_arc][:caption]}"

		if @current_arc < @archive_props.size 

			debug_info "SCAN ARC:: Next query arc"

			scan_current_archive
		end

	rescue Exception => ex

		debug_error ex.message
	end


	def prepare_arc_writer

		writer = DbArcWriter.new
		writer.db = get_db
		writer.cfg = @config
		writer
	end


	def get_db 

		@db ||= 		
			Sequel.connect(
				@cfg[:db][:connstr]
		)
	end

end

