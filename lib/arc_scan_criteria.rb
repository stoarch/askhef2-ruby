class ArcScanCriteria

  attr_accessor :date_query

  attr_accessor :count
  attr_accessor :kind
	attr_accessor :name
  attr_accessor :arc_caption
  attr_accessor :arc_code
end
