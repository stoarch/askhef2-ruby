
# This class query vzljot opc values and return string of res
#
# == Info
#  Execution of vzopc results as string list. Then it's 
#   received unchanged (for this version)
#
class VzopcQuerier

	attr_accessor :config

	def query( parm_capt )

    debug_info "VZOPC:: query #{parm_capt}"

		@lines = []

		opc_scaner = @config[:scaners][:opc]

		IO.popen( "#{opc_scaner} #{parm_capt}" ) do |io|

			@lines << io.readlines

		end

		@lines
	end
end
