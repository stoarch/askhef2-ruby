class AccessDb

  attr_accessor :mdb, :connection, :data, :fields

  def initialize( amdb )

    @mdb = amdb
    @connection = nil
    @data = nil
    @fields = nil
  end

  
  def open
    
    connection_string = 'Provider=Microsoft.Jet.OLEDB.4.0; Data Source='
    connection_string << @mdb

    @connection = WIN32OLE.new('ADODB.Connection')
    @connection.Open( connection_string )
  end


  def query( sql )

    recordset = WIN32OLE.new('ADODB.Recordset')
    recordset.Open( sql, @connection )
    @fields = []

    recordset.Fields.each do |field|

      @fields << field.name
    end

    begin

      @data = recordset.GetRows.transpose
    rescue

      @data = []
    end

  ensure

    recordset.Close if recordset.State == 1 #opened
  end


  def execute( sql )

    @connection.Execute( sql ) if @connection
  end


  def close

    @connection.Close if @connection
  end
end
