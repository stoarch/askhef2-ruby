
module MathTools

	class << self

		def aNAN
			s, e, m = rand(2), 2047, rand(2**52-1)+1
			[sprintf("%1b%011b%052b", s,e,m)].pack("B*").unpack("G").first
		end


		def is_a_number?(s)

			s.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true

		end


		def number?(object)

			Float(object) 

			true

		rescue 
			
			false

		end
	end
end
