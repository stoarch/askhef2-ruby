# encoding: utf-8

## Archive ##
#
# goal: Store the archive header and rows data. Also store the ids
#
class Archive

  attr_accessor :id, :caption #todo: move to model

  attr_accessor :header
  attr_accessor :rows

  def initialize

    @id = 0
    @caption = 'none'
    @header = Header.new 
    @rows = [] 
  end

	class Header

		attr :columns
    attr_accessor :arc_id

		class << self

			def make_from( arc_id, col_names )
				
				res = Header.new
				
				col_names.each_with_index do |cn, i|

					res.columns << Column.make_from( i + 1, arc_id, cn )
				end

				res
			end
		end

		def initialize

			@columns = []
      @arc_id = 0
		end

    def columns=( hash_ary )

      raise ArgumentError.new "Must be array of hashes, but #{hash_ary.class}" unless hash_ary.is_a? Array
      raise ArgumentError.new "Array need to have at least one item, but have #{hash_ary.size}" if hash_ary.size == 0
      raise ArgumentError.new 'Item 0 is not hash' unless hash_ary[0].is_a? Hash
      
      @columns = []

      hash_ary.each do |v|

        @columns << Column.make_from( v[:id], arc_id, v[:caption] )
      end
    end

		class Column

			attr_accessor :id
			#todo: move arc_id to header`
			attr_accessor :arc_id
			attr_accessor :caption

      def initialize( &block )

        yield self if block_given?
      end

			class << self

				def make_from( id, arc_id, caption )

					res = Column.new
					res.id = id
					res.arc_id = arc_id
					res.caption = caption
					res
				end
			end
		end
	end


	#^^^^^^^#
	## ROW ##
	#########

	class Row

		attr_accessor :id
		attr_accessor :cells
		attr_accessor :cur_date

		class << self

			def make_from( arc_id, row_id, cur_date, columns, cell_vals )

				raise ArgumentError, "arc_id must be integer, but #{arc_id}" unless arc_id.is_a? Integer 
				raise ArgumentError, "row_id must be integer, but #{row_id}" unless row_id.is_a? Integer 
				raise ArgumentError, "cur_date must be date, but #{cur_date}" unless ( cur_date.is_a? Date ) || ( cur_date.is_a? DateTime )

				raise ArgumentError, "columns must be non empty array of Archive::Header::Column, but #{columns}" unless columns && ( columns.is_a? Array ) && ( columns.size > 0 ) && ( columns[0].is_a? Archive::Header::Column )

				raise ArgumentError, "cell_vals must be non empty array of objects, but #{cell_vals}" unless cell_vals && ( cell_vals.is_a? Array ) && ( cell_vals.size > 0 )

				raise ArgumentError, "cell_vals size #{cell_vals.size} must be equal to size of columns #{columns.size}" unless cell_vals.size == columns.size


				res = Row.new
				res.id = row_id
				res.cur_date = cur_date
				
				cell_vals.each_with_index do |cv, i|

					res.cells << Cell.make_from( i + 1, arc_id, columns[i], cv )
				end

				res
			end
		end

		def initialize

			@cells = []
			@id = 0
		end


		#^^^^^^^^#
		## CELL ##
		##########

		class Cell

			attr_accessor :id
			attr_accessor :arc_id
			attr_accessor :column
			attr_accessor :value

      def initialize

        id = 0
        arc_id = 0
        column = Archive::Header::Column.new
        value = 0.0
      end

			class << self

				def make_from( id, arc_id, col, value )

					res = Cell.new
					res.id = id
					res.arc_id = arc_id
					res.column = col
					res.value = value
					res
				end
			end
		end

	end
end
