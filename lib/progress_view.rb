# encoding: utf-8

## ProgressView
#
# goal: Display animated image on screen with 10fps
#
class ProgressView
    attr :x, :y

    def initialize(animation, x, y)
        @x = x
        @y = y
        @animation = animation
        @color = Gosu::Color.new( 0xFFFFFF )
    end

    def draw
        img = @animation[Gosu::milliseconds / 100 % @animation.size]
        img.draw( @x - img.width/2.0, @y - img.height/2.0, 1, 1, 1) 
    end
end
