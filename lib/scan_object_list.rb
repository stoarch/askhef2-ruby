
class ScanObjectList 
	include Enumerable


	attr_accessor :cfg



	def initialize( acfg )

		@cfg = acfg

		init_objects

		reset

	end


	def each &block

		@objects.each &block

	end

	def reset

		@current_object_id = 0

	end

	def next

		@current_object_id += 1

		debug_info "SCAN OBJS::#{@objects[@current_object_id]} : #{current} of #{@objects.size}"

	end


	def at_start?

		@current_object_id == 0
		
	end


	def at_end?

		@current_object_id >= @objects.size

	end

	def has_more?

		! at_end?

	end


	def current

		@objects[ @current_object_id ]

	end


	private #
	######### AREA


	def init_objects

		@objects = @cfg[:objects].map{|ob| ScanObject.new( ob[1][:id], ob[1][:caption], cfg )}    

		debug_info @objects

	end


	def to_s

		@objects.inject(''){|summ, o| o.caption }

	end

end

