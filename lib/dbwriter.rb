# encoding: utf-8
# This class provide asynchronous writing of values to db
#
# == Info
#  Underlying db is specified by config
#

class DbWriterError 

	attr :message

	def initialize( msg )
		@message = msg
	end
end

class DBWriter

    include Celluloid

    attr_accessor :cfg
    attr_accessor :values

    attr :is_saving
    alias :is_saving? :is_saving

    def execute

        @is_saving = true

        $logger.info "DbWriter:: Start saving... #{values}"
        $logger.debug values.inspect

        prepare_values

        @db ||= connect_to_db

        @db.transaction do

            @values.each do |parm|

                @db[:values].insert(

                    meter_id: parm.obj_id, 

                    value: parm.value, 

                    date_received: parm.date_query, 

                    is_good: !( parm.is_good =~ /Good/ ).nil?, 

                    chanel: parm.chanel_id, 

                    parm_kind: @cfg[:param_kinds][parm.parm_kind.to_sym][:id]

                )
            end
        end


        $logger.info "DbWriter:: Saving done"

    rescue Exception => ex

      debug_error "DbWriter:: Error #{ex}"

			Actor[:message_board].mailbox << DbWriterError.new( ex.message ) if Actor[:message_board].alive?

    ensure

        @is_saving = false

        @db.disconnect if @db
        @db = nil

    end

    private


    def prepare_values


			$logger.debug @cfg[:params]

        @values.each do |parm|
					 

            parm_cfg = @cfg[:params].find{|f| f[:caption] == parm.caption}

						$logger.debug "DbWriter: parm_cfg: #{parm_cfg}"
            $logger.debug "DbWriter: parm #{parm}"

            next if parm.nil?             
            next if parm_cfg.nil?
            next if parm_cfg[:object].nil?

            parm.obj_id = @cfg[:objects][parm_cfg[:object].to_sym][:id]
            parm.chanel_id = parm_cfg[:chanel]
            parm.parm_kind = parm_cfg[:parm_kind]
        end
    end

    def connect_to_db

        Sequel.connect(
            @cfg[:db][:connstr]
        )
    end
end
