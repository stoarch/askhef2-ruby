# encoding: utf-8

# Display strings for specific areas
#
# == Areas
#   
#    Error area:: For exception messages
#  Scan status area:: For displaying current scanner status
#  Scan complete area:: For displaying that scanner finishes
#    Timer status area:: For displaying current time before query
#  Parms queue area:: Display last parms in area
#
class ScreenView < UT::Viewport

    def initialize( args )
        super args
    end


    # = draw_list
    #
    #  Display list in multicolumns. Parms specify width and height of list
    #
    def draw_list( list_ary, x, y, lines_cnt, cols_cnt, cur_obj )


        catch (:no_more_items) do 

            i = 0

            (0...cols_cnt).each do |c|
                (0...lines_cnt).each do |l|

                    throw :no_more_items if list_ary.empty?

                    put_string x + c*OBJ_COL_SIZE + OBJ_LIST_COL_SHIFT, y + l, list_ary.shift[0...OBJ_COL_SIZE], Gosu::Color::GRAY, Gosu::Color::BLACK if i != cur_obj

                    put_string x + c*OBJ_COL_SIZE + OBJ_LIST_COL_SHIFT, y + l, list_ary.shift[0...OBJ_COL_SIZE], Gosu::Color::WHITE, Gosu::Color::BLACK if i == cur_obj
                    i += 1
                end
            end

            put_string x + cols_cnt - 1, y + lines_cnt, "...", Gosu::Color::GRAY, Gosu::Color::BLACK
        end
    end



    # = show_arc_timer_status
    #
    # Display arc time to next query in seconds
    #
    def show_arc_timer_status( time )

        put_string ARC_TIMER_LEFT, ARC_TIMER_TOP, "Next arc query in #{time} seconds    ",Gosu::Color::GREEN, Gosu::Color::BLACK

    end


    # = show_timer_status
    #
    # Display time to next query in seconds
    #
    def show_timer_status( time )

        put_string TIMER_LEFT, TIMER_TOP, "Next query in #{time} seconds    ",Gosu::Color::GREEN, Gosu::Color::BLACK

    end


    # = show_parm_at
    #
    # Display value of parm data received from device
    #
    def show_parm_at( line, parm )

			debug_info "SCREEN VIEW:: show parm #{parm} at #{line} "

        put_string 0, line, parm.caption[0..19], Gosu::Color::GRAY, Gosu::Color::BLACK
        put_string 21, line, parm.value.to_s[0..7], Gosu::Color::GRAY, Gosu::Color::BLACK
        put_string 30, line, parm.is_good.to_s[0..4], Gosu::Color::GRAY, Gosu::Color::BLACK
        put_string 35, line, parm.date_query.strftime('%H:%M'), Gosu::Color::GRAY, Gosu::Color::BLACK
    end



    # = show_received_parms
    #
    # Display last received parms for status
    #
    def show_received_parms( parms )

			debug_info "SCREEN VIEW:: show received parms: #{parms}"

        @parm_queue_line = PARM_QUEUE_LINE 

        (parms.size-[PARM_QUEUE_HEIGHT,parms.size].min).upto( parms.size-1 )do |i|

            show_parm_at( @parm_queue_line, parms[i] )

            @parm_queue_line += 1

        end
    end


    # = show_error_msg
    #
    # Display error message in red or if none - in green
    #
    def show_error_msg(ex)

        put_string 0, ERROR_LINE, "Error: #{ex}"[0..VIEWPORT_WIDTH-1], Gosu::Color::RED, Gosu::Color::BLACK unless ex =~ /None/

        put_string 0, ERROR_LINE, "Error: #{ex}", Gosu::Color::GREEN, Gosu::Color::BLACK if ex =~ /None/
    end


    # = show_scan_status
    #
    # Display status of scanning (yellow switch)
    #
    def show_scan_status

        put_string 0, SCAN_STATUS_LINE, "Data scaning", Gosu::Color::YELLOW, Gosu::Color::BLACK
        put_string 0, SCAN_COMPLETE_LINE, "Data received", Gosu::Color::GRAY, Gosu::Color::BLACK
    end


    # = hide_scan_status 
    #
    # Hide status of scanning, displaying, that data is ready
    #
    def hide_scan_status 

        put_string 0, SCAN_STATUS_LINE, "Data scaning", Gosu::Color::GRAY, Gosu::Color::BLACK
        put_string 0, SCAN_COMPLETE_LINE, "Data received", Gosu::Color::GREEN, Gosu::Color::BLACK
    end


    # = show_arc_scan_status
    #
    # Display status of archive scanning (yellow switch)
    #
    def show_arc_scan_status

        put_string 0, SCAN_ARC_STATUS_LINE, "Scaning archives", Gosu::Color::YELLOW, Gosu::Color::BLACK
        put_string 0, SCAN_ARC_COMPLETE_LINE, "Archives received", Gosu::Color::GRAY, Gosu::Color::BLACK
    end


    # = hide_arc_scan_status 
    #
    # Hide status of scanning, displaying, that data is ready
    #
    def hide_arc_scan_status 

        put_string 0, SCAN_ARC_STATUS_LINE, "Scaning archives", Gosu::Color::GRAY, Gosu::Color::BLACK
        put_string 0, SCAN_ARC_COMPLETE_LINE, "Archives received", Gosu::Color::GREEN, Gosu::Color::BLACK
    end


    # = show_save_status
    #
    # Display status of data saving started (yellow)
    #
    def show_save_status
        put_string SAVE_STATUS_COL, SAVE_STATUS_LINE, "Saving...", Gosu::Color::YELLOW, Gosu::Color::BLACK
        put_string SAVE_STATUS_COL, SAVE_COMPLETE_LINE, "Saving done", Gosu::Color::GRAY, Gosu::Color::BLACK
    end


    # = show_save_end_status
    #
    # Hide status, that data saving in progress (gray)
    #
    def show_save_end_status
        put_string SAVE_STATUS_COL, SAVE_STATUS_LINE, "Saving...", Gosu::Color::GRAY, Gosu::Color::BLACK
        put_string SAVE_STATUS_COL, SAVE_COMPLETE_LINE, "Saving done", Gosu::Color::GREEN, Gosu::Color::BLACK
    end

end



