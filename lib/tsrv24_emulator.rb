## = TSRV 24 Emulator software
#
#  Goal: Emulate the Vzljot TSRV24 requests and answers (modbus)
#				for specific COM port
#
#	 Info:
#		
#		1. If you want to use emulator on the same pc, where you placed
#			you program. Then use com0com emulator (
#
#
require 'logger'
require 'yamler'
require 'ut'

TILE_SIZE = 16
SCALE_X = 1.85
SCALE_Y = 1.25
WINDOW_WIDTH = 800 
WINDOW_HEIGHT = 600 

VIEWPORT_WIDTH = WINDOW_WIDTH/TILE_SIZE
VIEWPORT_HEIGHT = WINDOW_HEIGHT/TILE_SIZE


## ....... ##
#  CLASSES  #
#############


class ScreenView < UT::Viewport

    def initialize( args )
        super args
    end
end


## == Window
#
# goal: Display main emulator window and interact with user
#
# funcs:
#		init ui:: Make window and display on screen
#		display query:: Display clients query strings
#		display answer:: Display our answer to clients
#		display clients:: Display client connection status
#
class Window < Gosu::Window

	attr_accessor :viewport

	def initialize( width, height )

		super width, height, false

		caption = 'TSRV24 emulator'
	end


  def button_down( id )
    close if id == Gosu::KbEscape
  end

	def draw

		$viewport.draw
	end

	def update

	end
end


LOGOUT ||= 'tsrv24-emu.log'

## ......... ##
#  MAIN CODE  #
###############

$logger = Logger.new LOGOUT, 'daily'

$logger.info 'Emulator started...'

$window = Window.new WINDOW_WIDTH, WINDOW_HEIGHT


@renderer = UT::FontRenderer.new font_name: 'fonts/DejaVuSansMono.ttf', tile_size: TILE_SIZE

$viewport = ScreenView.new renderer: @renderer, width: VIEWPORT_WIDTH, height: VIEWPORT_HEIGHT

## DISPLAY TITLE ##

$viewport.put_string 0, 0, "TSRV24 Emulator", Gosu::Color::WHITE
$viewport.put_string 0, 1, "Copyright (c) 2013 by Afonin Vladimir", Gosu::Color::GRAY

## INIT WINDOW ##

$window.viewport =  $viewport


## RUN ##
$window.show unless $testing

$logger.info 'Server stopped.'
